//
//  FloorTileProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/2/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol FloorTileProtocol
{
    var Sprite : SKSpriteNode { get set }


}