//
//  ShipProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol ShipProtocol {
    var ShipName : String { get set }
    var weapons : [WeaponProtocol] { get set }
    var BaseNode : SKNode { get set }
    var sprite : SKSpriteNode { get set }
    var VelocityMultiplier : Double { get set }
    
    var Orbitals : [OrbitalProtocol] { get set } 
    
    func AttemptToFire(weaponNumber : Int) -> [ProjectileProtocol]
}