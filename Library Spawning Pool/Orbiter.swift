//
//  Orbiter.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/3/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class Orbiter : ShipProtocol {
    var ShipName : String
    var weapons : [WeaponProtocol]
    var BaseNode : SKNode = SKNode()
    var sprite : SKSpriteNode
    var VelocityMultiplier : Double
    var Orbitals : [OrbitalProtocol] = []
    var AuxNodes : [SKNode] = []
    init()
    {
        ShipName = "Orbiter"
        weapons = []
        sprite = SKSpriteNode(imageNamed: "enemyBlack5.png")
        BaseNode.addChild(sprite)
        VelocityMultiplier = 1.0
        let numShips = 5
        let zRotation : CGFloat = CGFloat(6.28314 / Double(numShips))
        for i in 0..<numShips
        {
            let fighter = FighterShuttles(offset: i, rotation: zRotation)
            
            sprite.addChild(fighter.Base)
            Orbitals.append(fighter)
        }
    }
    func AttemptToFire(weaponNumber : Int) -> [ProjectileProtocol] {
        
        for orb in Orbitals {
            var orbProjectiles = orb.Attack()
            
            let rad = orb.OrbitRadius
            
            let debugString = String(format: "orb zRotation : %f ", orb.Base.zRotation % 6.283184)
            
            println("Debug String: \(debugString)")
        }
        
        var projectiles : [ProjectileProtocol] = []
        
        
        
        for aux in AuxNodes {
//            println(aux.zRotation)
        }
        if(weaponNumber < 0)
        {
            return projectiles
        }
        if weaponNumber < weapons.count
        {
            var weapon = weapons[weaponNumber]
            projectiles = weapon.getProjectile()
        }
        else
        {
            projectiles = AttemptToFire(weaponNumber - 1)
        }
        
        return projectiles
    }

}