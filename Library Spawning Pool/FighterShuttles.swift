//
//  FighterShuttles.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/10/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class FighterShuttles : OrbitalProtocol {
    var Sprite : SKSpriteNode
    var Weapons : [WeaponProtocol]
    var OrbitRadius : Double
    var Base : SKNode
    
    init(offset : Int, rotation : CGFloat) {
        Sprite = SKSpriteNode(imageNamed: "enemyBlack2.png")
        Weapons = [SingleLaser()]
        OrbitRadius = 100.0
        Base = SKNode()
        Sprite.position.x = CGFloat(OrbitRadius)
        
        Sprite.setScale(0.5)
        
        // rotate base
        let rotate = SKAction.rotateByAngle(1.570796, duration: 1.5)
        let repeat = SKAction.repeatActionForever(rotate);
        Base.zRotation = rotation * CGFloat(offset)
        Base.runAction(repeat)
        
        //rotate sprite
        Sprite.runAction(SKAction.repeatActionForever(
            SKAction.rotateByAngle(-1.570796, duration: 1.5)
            ))
        
        Sprite.zRotation = -rotation * CGFloat(offset)
        
        Base.addChild(Sprite)
    }
    
    func Attack() -> [ProjectileProtocol] {
        let newAttack = Weapons[0].getProjectile()
        
        var projectiles : [ProjectileProtocol] = []
        
        for proj in newAttack {
            
//            self.Sprite.addChild(proj.Sprite);
//            proj.Sprite.runAction(SKAction.moveBy(CGVector(dx: 0.0, dy: -2000.0), duration: 2.0));
            
            projectiles.append(proj)
            
//            let projNode = SKNode()
//            
//            projNode.addChild(proj.Sprite)
//            self.Sprite.addChild(projNode)
//            projNode.zRotation = self.Sprite.zRotation;
//            proj.Sprite.runAction(SKAction.moveBy(CGVector(dx: -1000.0, dy: 0.0), duration: 2.0))
            
        }
        return projectiles;
    }
}