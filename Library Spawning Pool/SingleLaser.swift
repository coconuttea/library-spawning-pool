//
//  SingleLaser.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
class SingleLaser : WeaponProtocol, VehicalLibraryBaseProtocol {
    var Name : String
    var Ammo : Int = 9999
    var Speed : Double
    var Offset : [CGPoint]
    var VelocityOffset = [CGPoint(x: 0.0, y:0.0)]
    init()
    {
        Name = "Single Laser"
        Speed = 2.0

        Offset = [CGPoint(x: 0.0, y: 0.0)]
    }
    
    func getProjectile() -> [ProjectileProtocol] {
        
        if(Ammo <= 0)
        {
            return []
        }
        Ammo -= 1;
        var projectile = SingleLaserProjectile(offset: CGPoint(x: 0.0, y: 0.0), VelocityOffset : VelocityOffset[0])
        return [projectile]
    }
    
    func getNewInstance() -> VehicalLibraryBaseProtocol {
        return SingleLaser()
    }
    
}