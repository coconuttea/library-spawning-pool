//
//  TextBoxWidthTestingBubble.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/5/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class TextBoxWidthTestingBubble : SKNode, BubbleProtocol
{
    var text : String
    var duration : Double
    var sprites : [SKNode]
    var Node : SKNode = SKNode()
    var AtoZ : [String]
    override init()
    {
        text = ""
        duration = 10.0
        sprites = []
        
        AtoZ = [
            "A", // 17
            "B", // 16
            "C", // 14
            "D", // 15
            "E", // 18
            "F", // 20
            "G", // 13
            "H", // 15
            "I", // 90?
            "J", // 21
            "K", // 16
            "L", // 20
            "M", // 12
            "N", // 15
            "O", // 13
            "P", // 17
            "Q", // 13
            "R", // 16
            "S", // 16
            "T", // 19
            "U", // 15
            "V", // 16
            "W", // 11
            "X", // 18
            "Y", // 17
            "Z", // 19
            " ", // 33
            ".", // 33
            ",", // 33
            ":", // 33
            "!"  // 50
            
        ]
        super.init()
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func runActions()
    {
        parseText()
        
        let wait = SKAction.waitForDuration(20.0)
        let remove = SKAction.runBlock({self.removeFromParent()})
        let seq = SKAction.sequence([wait, remove])
        
        self.runAction(seq)
    }

    func parseText() {
        for i in 20..<AtoZ.count {
            var curLabelText = "."
            for j in 0..<36 {
                curLabelText += AtoZ[i]
            }
            let newLabel = SKLabelNode(text: curLabelText.lowercaseString + "STOP")
            newLabel.position.y = CGFloat(-30.0 * Double(i - 20))
            newLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
            self.addChild(newLabel)
        }
        
        
    }
}