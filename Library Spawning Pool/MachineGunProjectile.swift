//
//  MachineGunProjectile.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/6/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class MachineGunProjectile : ProjectileProtocol
{
    var BaseNode : SKNode
    var Sprite : SKSpriteNode
    var Damage : Double = 10.0
    var Speed : Double = 1.0
    var Crit : Bool = false
    var CritChance : Double = 25.0
    var CritModifier : Double = 2.0
    var Offset : CGPoint
    var VelocityOffset : CGPoint
    init(offset : CGPoint, VelocityOffset : CGPoint)
    {
        BaseNode = SKNode()
        let SpriteImage = "laserRed13.png"
        Sprite = SKSpriteNode(imageNamed: SpriteImage)
        Offset = offset
        self.VelocityOffset = VelocityOffset
        //        let move = SKAction.moveBy(CGVector(dx: offsetCoef, dy: 0.0), duration: 0.001)
        //        offsetCoef *= -1.0
        //        sprite.runAction(move)
        
        if( (Double(random()) % 100) <= CritChance)
        {
            Crit = true
            Damage *= CritModifier
        }
        
    }
}
