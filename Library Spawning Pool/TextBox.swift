//
//  TextBox.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/3/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class TextBox : DialogueBoxProtocol {
    //should be enforced to whatever teh BG Size is or vice versa or mabye or arbitrary enforcement
    var textLabel : [SKLabelNode]
    var textLabelBackground : SKSpriteNode

    
    init() {
        textLabel = []
        textLabelBackground = SKSpriteNode()
    }
}