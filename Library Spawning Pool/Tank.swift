//
//  Tank.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/14/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class Tank : ShipProtocol {
    var ShipName : String
    var weapons : [WeaponProtocol]
    var BaseNode : SKNode = SKNode()
    var sprite : SKSpriteNode
    var VelocityMultiplier : Double
    var Orbitals : [OrbitalProtocol] = []
    
    init() {
        ShipName = "Beige Tank"
        weapons = [SingleLaser()]
        
//        let body = SKSpriteNode(imageNamed: "tankBeige_outline.png")
//        BaseNode.addChild(body)
        
        VelocityMultiplier = 0.5
        
//        sprite = SKSpriteNode(imageNamed: "barrelBeige.png");
        sprite = SKSpriteNode(imageNamed: "tankBeige_outline.png")
//        sprite.anchorPoint = CGPoint(x: 0.5, y: 0.1)
        
        BaseNode.addChild(sprite)
        
    }
    
    func AttemptToFire(weaponNumber: Int) -> [ProjectileProtocol] {
        var projectiles : [ProjectileProtocol] = []
        
        if(weaponNumber < weapons.count)
        {
            let weapon = weapons[weaponNumber]
            projectiles = weapon.getProjectile();
        }
        
        return projectiles
    }
}