//
//  UsersWebServiceConnector.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/27/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
class UsersWebServiceConnector : NSURLConnection, NSURLConnectionDataDelegate {
    var downloadedData : NSMutableData = NSMutableData()
    var friends = NSMutableArray();
    
    func Connect() {
        let url = NSURL(string: "jonathanwong.local/users.php")
        var request = NSURLRequest(URL: url!);
        var connection = NSURLConnection(request: request, delegate: self, startImmediately: true);
        
        var session = NSURLSession.sharedSession();
        
        let task = session.dataTaskWithRequest(request, completionHandler: {
            data, response, error -> Void in
            var payload = NSString(data: data, encoding: NSUTF8StringEncoding)
            let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves, error: nil) as! NSMutableArray
            
        })
        
        task.resume();
    }
    

    
}
