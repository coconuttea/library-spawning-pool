//
//  Answer.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/4/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

class Answer {
    var text: String
    var correct : Bool
    var points: Int
    
    init(text : String, correct: Bool, points: Int) {
        self.text = text
        self.correct = correct
        self.points = points
    }
}