//
//  CardProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/23/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol CardProtocol {
    var Type : String { get set }
    var Name : String { get set }
    var Value : String { get set }
    var AlternateValue : String { get set }
    var FaceDown : Bool { get set }
    var BaseNode : SKNode { get set }
    var CardBack : SKSpriteNode { get set }
    var CardSprite : SKSpriteNode { get set }
    func Flip()
}