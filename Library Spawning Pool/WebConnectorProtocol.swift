//
//  WebConnectorProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/26/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

protocol WebConnectorProtocol {
    func Connect()
    
}