//
//  TruckWebServiceConnector.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 9/22/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

class TruckWebServiceConnector : NSURLConnection, NSURLConnectionDataDelegate {
    var downloadedData : NSMutableData = NSMutableData()
    var trucks = NSMutableArray();
    
    func Connect() {
        let url = NSURL(string: "http://jonathanwong.local/MaximunchOverdrive/trucks.php")
        var request = NSURLRequest(URL: url!);
        var connection = NSURLConnection(request: request, delegate: self, startImmediately: true);
        
        var session = NSURLSession.sharedSession();
        
        let task = session.dataTaskWithRequest(request, completionHandler: {
            data, response, error -> Void in
            var payload = NSString(data: data, encoding: NSUTF8StringEncoding)
            let json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves, error: nil) as! NSMutableArray
            println(json);
        })
        
        task.resume();
    }

}