//
//  DamageBubble.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/4/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class DamageBubble : BubbleProtocol {
    var text : String
    var duration : Double
    var Crit : Bool = false
    var sprites : [SKNode]
    var Node : SKNode = SKNode()
    var label = SKLabelNode(text : "")
    var explosionImages : [String] =
    [
        "explosion00.png",
        "explosion01.png",
        "explosion02.png",
        "explosion03.png",
        "explosion04.png",
        "explosion05.png",
        "explosion06.png",
        "explosion07.png",
        "explosion08.png"
    ]
    init(text : String) {
        self.text = text;
        duration = 1.0
        sprites = []
//        sprites.append(label)
        label = SKLabelNode(text : text)
        Node.addChild(label)
//        let face = SKSpriteNode(imageNamed: "angry.gif");
//        face.setScale(3.0)
//        self.addChild(face)
    }
    
    func runActions() {

        label.removeFromParent()
        
        label = SKLabelNode(text : text)
        label.fontColor = UIColor(red: 1.0, green: 1.0, blue: 0.2, alpha: 1.0)
        label.fontName  = "HelveticaNeue"
        label.zPosition = 1
        if(Crit)
        {
            label.fontName = "HelveticaNeue-Bold"
            label.fontSize = 40.0
            label.fontColor = UIColor(red: 1.0, green: 0.2, blue: 0.2, alpha: 1.0)
            label.text += "!"
            label.zPosition = 2
        }
        
        Node.addChild(label)
        label.runAction(SKAction.sequence([
            SKAction.moveBy(CGVector(dx: 0.0, dy: 100.0), duration: duration),
            SKAction.runBlock({self.Node.removeFromParent()})
            ]))
        
        let explosionImage = explosionImages[random() % 9]
        let explosion = SKSpriteNode(imageNamed: explosionImage)
        explosion.anchorPoint = CGPoint(x: 0.5, y: 0.0)
        explosion.setScale(0.09)
        
        let delay = SKAction.waitForDuration(0.08)
        let die = SKAction.runBlock({explosion.removeFromParent()})
        let expSeq = SKAction.sequence([delay, die])
        
        explosion.runAction(
            expSeq
        )
        
        Node.addChild(explosion)
        
//        Node.runAction(SKAction.runBlock({
//            self.label.color = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
//        }));
    }
    
}