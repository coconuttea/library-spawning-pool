//
//  PlayerStatisticsTracker.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 10/21/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class PlayerStatisticsTracker : SKNode {
    
    var HealthLabel : SKLabelNode = SKLabelNode()
    var smallIncrement : SKSpriteNode = SKSpriteNode()
    var bigIncrement : SKSpriteNode = SKSpriteNode()
    var smallDecrement : SKSpriteNode = SKSpriteNode()
    var bigDecrement : SKSpriteNode = SKSpriteNode()
    var nameLabel : SKLabelNode = SKLabelNode()
    
    // set to <= 0 if infinite
    var maxHp : Int64 = 0
    
    var currentHp : Int64 = 0
    
    override init() {
        super.init();
        HealthLabel.text = "0";
        HealthLabel.fontColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
//        self.addChild(HealthLabel);
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func updateCurrentHpBy(newHpModifier : Int64) {
        if(maxHp <= 0 || currentHp + newHpModifier <= maxHp) {
            currentHp = currentHp + newHpModifier
        }
        else if (newHpModifier > 0) {
            currentHp = maxHp;
        }
        else {
            currentHp = currentHp + newHpModifier;
        }
        
//        HealthLabel.removeFromParent();
        HealthLabel.text = "\(currentHp)"
    }
    
    func updateMaxHpBy(newMaxHpModifier : Int64) {
        maxHp = maxHp + newMaxHpModifier
    }
}