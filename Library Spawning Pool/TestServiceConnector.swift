//
//  TestServiceConnector.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/18/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

class TestServiceConnector : NSURLConnection, NSURLConnectionDataDelegate {
    
    var downloadedData : NSMutableData = NSMutableData()
    func Connect() {
        let id = 1
        var con = NSURL(string: "http://jonathanwong.local/test.php");
        var request = NSMutableURLRequest(URL: con!)
        
        var connection = NSURLConnection(request: request, delegate: self, startImmediately: true);
        
        request.HTTPMethod = "POST";
        
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
//        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        
//        var params = Dictionary<String, String>();
        var params = NSMutableDictionary()
//        params.updateValue("123", forKey: "key1")
        params.setValue("hello", forKey: "world")
        
        
//        var params = "id" : "1"  as Dictionary<String, String>()
    
//        let body = NSJSONSerialization.dataWithJSONObject(params, options: NSJSONWritingOptions.PrettyPrinted, error: nil);
        let postString = "id=10&name=this";
        
        let body = postString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        
        
        request.HTTPBody = body;
        
        var response : AutoreleasingUnsafeMutablePointer<NSURLResponse?> = nil
        
//        NSURLConnection.sendSynchronousRequest(request, returningResponse: response, error: nil)
        
        let session = NSURLSession.sharedSession()
        
        var payload : NSString = ""
        var postDataTask = session.dataTaskWithRequest(request, completionHandler:
            {
                data, response, error -> Void in
                var d = NSString(data: data, encoding: NSUTF8StringEncoding)
                println("postDataTask")
                println(d!)
                
            })
            
        postDataTask.resume()
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
//        println("did finish loading");
    }
    
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
//        println("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
//        println(data.description);
//        println(data)
        downloadedData.appendData(data)
        var error : NSError
        
//        var json : NSMutableArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSMutableArray
//        var json : NSMutableDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSMutableDictionary;
        
        var json = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments, error: nil)
        
        //        for value in json {
        //            println("\(value.key) : \(value.value)");
        //        }
        
        
//        var s = NSString(data: data, encoding: NSUTF8StringEncoding)
        
//        println("postdata \(s!)")
    }
    
    //    func connectionDidFinishLoading(connection: NSURLConnection!) {
    //        println("finished loading")
    //    }
    
    
}