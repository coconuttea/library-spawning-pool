//
//  MachineGun.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/6/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class MachineGun : WeaponProtocol, VehicalLibraryBaseProtocol {
    var Name : String
    var Ammo : Int = 100
    var Speed : Double
    var Offset : [CGPoint]
    var VelocityOffset : [CGPoint]
    
    init() {
        Name = "Machine Gun"
        Speed = 1.5
        
        Offset = [
//            CGPoint(x: 0.0, y: -0.0),
//            CGPoint(x: 7.0, y: -60.0),
//            CGPoint(x: -7.0, y:-60.0),
//            CGPoint(x: 0.5, y:-90.6),
//            CGPoint(x: 7.0, y: -60.5),
//            CGPoint(x: -7.0, y: 45.9)
            
            
            CGPoint(x: 0.0, y: -60.0),
            CGPoint(x: 0.0, y: -75.0),
            CGPoint(x: 0.0, y: 35.0),
            
            CGPoint(x: 3.0, y: -60.0),
            CGPoint(x: 3.0, y: -110.0),
            CGPoint(x: 3.0, y: 25.0),
            
            CGPoint(x: -3.0, y: -60.0),
            CGPoint(x: -3.0, y: -27.0),
            CGPoint(x: -3.0, y: 15.0)
        
        ]
        VelocityOffset = [
            CGPoint(x: 0.0, y: 0.0),
            CGPoint(x: 0.0, y: 0.0),
            CGPoint(x: 0.0, y: 0.0),
            CGPoint(x: 0.0, y: 0.0),
            CGPoint(x: 0.0, y: 0.0),
            CGPoint(x: 0.0, y: 0.0),
            CGPoint(x: 0.0, y: 0.0),
            CGPoint(x: 0.0, y: 0.0),
            CGPoint(x: 0.0, y: 0.0)
        ]
    }
    func getProjectile() -> [ProjectileProtocol] {
        var projectiles : [ProjectileProtocol] = []
        for i in 0..<6
        {
            let withOffset = (random() % 9)
            var projectile = MachineGunProjectile(offset: Offset[withOffset], VelocityOffset: VelocityOffset[i])
            projectile.Sprite.setScale(CGFloat(0.5))
            projectiles.append(projectile)
//            projectile.Sprite.runAction(SKAction.repeatActionForever(SKAction.rotateByAngle(3.14159, duration: 0.1)))
            
        }
        
        return projectiles
    }
    
    func getNewInstance() -> VehicalLibraryBaseProtocol {
        return MachineGun()
    }
}