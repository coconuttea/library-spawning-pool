//
//  PokerPlayer.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class PokerPlayer : PlayerProtocol {
    var Name : String
    var Board : [CardProtocol]
    var Hand : [CardProtocol]
    var Library : DeckProtocol
    var Graveyard : [CardProtocol]

    init() {
        Name = "Player1";
        Board = []
        Hand = []
        Library = StandardPlayingCardDeck()
        Graveyard = []
    }
}