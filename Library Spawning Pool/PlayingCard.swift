//
//  PlayingCard.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/23/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class PlayingCard : CardProtocol {
    var Name : String 
    var Value : String
    var AlternateValue : String
    var FaceDown : Bool
    var Type : String
    var BaseNode : SKNode
    var CardBack : SKSpriteNode = SKSpriteNode(imageNamed: "cardBack_blue1.png")
    var CardSprite : SKSpriteNode = SKSpriteNode();
    init() {
        Name = ""
        Value = ""
        AlternateValue = ""
        FaceDown = false
        Type = ""
        BaseNode = SKNode()
    }
    
    func Flip() {
        FaceDown = !FaceDown;
        
        if FaceDown {
            BaseNode.addChild(CardBack)
        }
        else {
            CardBack.removeFromParent()
        }
    }
}