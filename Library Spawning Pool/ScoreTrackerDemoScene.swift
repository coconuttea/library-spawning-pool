//
//  ScoreTrackerDemoScene.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 10/21/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class ScoreTrackerDemoScene  : SKScene {
    
    enum SwipeType : Int {
        case UP = 1,
        DOWN = 2,
        NONE = 0
    }
    
    var leftBase = SKNode();
    var rightBase = SKNode();
    var leftPlayer = PlayerStatisticsTracker()
    var rightPlayer = PlayerStatisticsTracker()
    override func didMoveToView(view: SKView) {
        backgroundColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        initializeScoreScreen()
        
        
        var swipeUpRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("swipedUp"));
        swipeUpRecognizer.direction = .Up
        self.view?.addGestureRecognizer(swipeUpRecognizer);
        
        let swipeDownRecognizer = UISwipeGestureRecognizer(target: self, action: Selector("swipedDown"));
        swipeDownRecognizer.direction = .Down;
        
        self.view?.addGestureRecognizer(swipeDownRecognizer);
    }
    

    
    func swipedUp()
    {
        println("Swiped up");
    }
    
    func swipedDown()
    {
        println("Swiped down");
    }
    
    func initializeScoreScreen() {
        leftPlayer.nameLabel.text = "Home"
        leftPlayer.maxHp = 0
        leftPlayer.currentHp = 20
        leftPlayer.updateCurrentHpBy(0);
        
//        updatePlayer(leftPlayer);
        
        leftPlayer.HealthLabel.fontColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        leftPlayer.HealthLabel.text = "\(leftPlayer.currentHp)";
        leftPlayer.HealthLabel.position.x = CGFloat(self.size.width / 4)
        leftPlayer.HealthLabel.position.y = CGFloat(self.size.height / 2)
        leftPlayer.HealthLabel.fontSize = CGFloat(72.0);
        
        leftPlayer.nameLabel.position.x = CGFloat(self.size.width / 4)
        leftPlayer.nameLabel.position.y = CGFloat(self.size.height * 0.75)
        leftPlayer.nameLabel.fontSize = CGFloat(72.0);
        leftPlayer.nameLabel.fontColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        leftPlayer.nameLabel.fontName = "HelveticaNeue-Bold"
        
        self.addChild(leftPlayer.nameLabel);
        self.addChild(leftPlayer.HealthLabel);
        
        
        
        
        rightPlayer.nameLabel.text = "Away"
        rightPlayer.maxHp = 0
        rightPlayer.currentHp = 20
        rightPlayer.updateCurrentHpBy(0);
        
        rightPlayer.HealthLabel.fontColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        rightPlayer.HealthLabel.text = "\(rightPlayer.currentHp)";
        rightPlayer.HealthLabel.position.x = CGFloat(self.size.width * 0.75)
        rightPlayer.HealthLabel.position.y = CGFloat(self.size.height * 0.50)
        rightPlayer.HealthLabel.fontSize = CGFloat(72.0);
        
        
        rightPlayer.nameLabel.position.x = CGFloat(self.size.width * 0.75)
        rightPlayer.nameLabel.position.y = CGFloat(self.size.height * 0.75)
        rightPlayer.nameLabel.fontSize = CGFloat(72.0);
        rightPlayer.nameLabel.fontColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        
        rightPlayer.nameLabel.fontName = "HelveticaNeue-Bold"
        
        self.addChild(rightPlayer.nameLabel)
        self.addChild(rightPlayer.HealthLabel);
        
    }
    
    func updatePlayer(player : PlayerStatisticsTracker) {
        
    }
    
    func makeBubble(hpModifier : Int64, location : CGPoint) {
        var bubbleColor = UIColor(red: 0.8, green: 0.0, blue: 0.0, alpha: 1.0)
        
        if(hpModifier > 0) {
            bubbleColor = UIColor(red: 0.0, green: 0.8, blue: 0.0, alpha: 1.0)
        }
        
        let bubble = OneLineMessageBubble(text: "\(hpModifier)");
        bubble.label.fontColor = bubbleColor
        bubble.label.fontSize = 72.0
        bubble.Node.position = location
        bubble.runActions()
        self.addChild(bubble.Node)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        var hpModifier : Int64 = 0;
        for touch in touches as! Set<UITouch>{
            var bubbleColor = UIColor(red: 0.0, green: 0.8, blue: 0.0, alpha: 1.0)
            if(touch.locationInNode(self).y > self.size.height / 2){
                hpModifier = 1
            } else
            {
                hpModifier = -1;
                bubbleColor = UIColor(red: 0.8, green: 0.0, blue: 0.0, alpha: 1.0)
            }
            
            
            
//            for recog in touch.gestureRecognizers {
//                if(recog is UISwipeGestureRecognizer && recog.direction == .Up)
//                {
//                    //                    println("Swiped up");
//                    hpModifier = 5
//                    bubbleColor = UIColor(red: 0.0, green: 0.8, blue: 0.0, alpha: 1.0)
//                }
//                if(recog is UISwipeGestureRecognizer && recog.direction == .Down) {
//                    //                    println("Swiped down");
//                    hpModifier = -5;
//                    bubbleColor = UIColor(red: 0.8, green: 0.0, blue: 0.0, alpha: 1.0)
//                }
//            }
//            
            let bubble = OneLineMessageBubble(text: "\(hpModifier)")
            bubble.label.fontColor = bubbleColor
            bubble.label.fontSize = CGFloat(72.0)
            
            //left player
            if(touch.locationInNode(self).x < self.size.width / 2) {
                leftPlayer.updateCurrentHpBy(hpModifier)
                
            } else {
                rightPlayer.updateCurrentHpBy(hpModifier)
            }
//            
//            bubble.Node.position = touch.locationInNode(self);
//            
//            bubble.runActions()
            
            
//            self.addChild(bubble.Node)
            makeBubble(hpModifier, location: touch.locationInNode(self));
            
            
    
       
    }
    
//        
//        leftPlayer.currentHp--;
//        leftPlayer.HealthLabel.text = "\(leftPlayer.currentHp)"
        
        
    }
}
