//
//  AvatarGeneratorProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/12/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol AvatarGeneratorProtocol {
    var Name : String { get set }
    var BaseNode : SKNode { get set }
    var Options : [SKLabelNode] { get set }
    
    func UpdateAvatar()
    
    
}