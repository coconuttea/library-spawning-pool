//
//  AvatarGenerator.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/12/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class AvatarGenerator : AvatarGeneratorProtocol {
    var Name : String
    var BaseNode : SKNode
    var Options : [SKLabelNode] = []
    init() {
        Name = "Girl1"
        BaseNode = SKNode()
        
        let components : [String] = [
            "avatar_head_f1",
            "avatar_eye_f2",
            "avatar_hair_f3",
            "avatar_mouth_f3"
            
        ]
        
        for component in components {
            var compSprite = SKSpriteNode(imageNamed: component)
            compSprite.anchorPoint = CGPoint(x: 0.5, y: 0.0)
            BaseNode.addChild(compSprite)
        }
        
        var compSprite = SKSpriteNode(imageNamed: "avatar_disabled")
        compSprite.position.y = -32.0
        BaseNode.addChild(compSprite);
        
        var i = 0;
        let offset : CGFloat = 30.0
        let optionStrings = [
        "Hello",
        "World"]
        
        for optionString in optionStrings {
            var textNode = SKLabelNode(text: optionString);
            
            textNode.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
            textNode.position.x = -32.0
            textNode.position.y = -offset * CGFloat(i)
            //        BaseNode.addChild(textNode)
            compSprite.addChild(textNode);
            Options.append(textNode)
            
            i++
        }
        
        
    }
    
    func UpdateAvatar() {
        
    }
}