//
//  WeaponProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
protocol WeaponProtocol {
    var Name : String { get set }
    var Ammo : Int { get set }
    var Offset : [CGPoint] { get set }
    var VelocityOffset : [CGPoint] { get set }
    func getProjectile() -> [ProjectileProtocol]
}