//
//  EnergyBall.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/6/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class EnergyBall : OrbitalProtocol {
    var Sprite : SKSpriteNode
    var Weapons : [WeaponProtocol]
    var OrbitRadius : Double
    var Base : SKNode
    
    init()
    {
        Sprite = SKSpriteNode(imageNamed: "laserGreen14.png")
        Weapons = []
        OrbitRadius = 105.0
        Base = SKNode()
    }
    
    func runActions()
    {
        let rotate = SKAction.rotateByAngle(1.570796, duration: 0.50)
        
//        let print = SKAction.runBlock({println(self.Rotation.zRotation) } );
        let seq = SKAction.sequence([rotate])
        let runForever = SKAction.repeatActionForever(seq)
//        Sprite.addChild(Rotation)
        Base.runAction(runForever)
//        let rep = SKSpriteNode(imageNamed: "laserGreen14.png")

        
        Base.addChild(Sprite)
        Sprite.position = CGPoint(x: OrbitRadius, y: 0.0)
        
        let ballSpin = SKAction.repeatActionForever(
            SKAction.rotateByAngle(1.570796, duration: 0.10)
            )
        Sprite.runAction(ballSpin)
        
    }
    func Attack() -> [ProjectileProtocol]{
        var projectiles : [ProjectileProtocol] = []
        for weapon in Weapons {
            for proj in weapon.getProjectile() {
                projectiles.append(proj)
            }

        }
        return projectiles
    }
}