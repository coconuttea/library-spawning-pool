//
//  ScoreTrackerSceneViewController.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 10/21/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import UIKit
import SpriteKit

extension SKNode {
    class func unarchiveScoreTrackerLibraryFromFile(file : String) -> SKNode? {
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as!
            ScoreTrackerDemoScene;
            
            archiver.finishDecoding();
            return scene;
        } else
        {
            return nil;
        }
        
    }
}

class ScoreTrackerViewController : UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        

        if let scene =
            ScoreTrackerDemoScene.unarchiveScoreTrackerLibraryFromFile("ScoreTrackerDemoScene") as? ScoreTrackerDemoScene {
                let skView = self.view as! SKView
                
                scene.scaleMode = .AspectFill
                skView.presentScene(scene)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}