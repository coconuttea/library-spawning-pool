//
//  Corsair.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
class Corsair : ShipProtocol {
    var name : String
    var weapons : [WeaponProtocol]
    var sprite : SKSpriteNode
    var VelocityMultiplier : Double
    init()
    {
        name = "Corsair"
        weapons = [SingleLaser(), DoubleLaser()]
        sprite = SKSpriteNode(imageNamed:"enemyBlack3.png")
        VelocityMultiplier = 1.5

    }
    
    func AttemptToFire(weaponNumber : Int) -> [ProjectileProtocol] {
        var projectiles : [ProjectileProtocol] = []
        if weaponNumber < weapons.count
        {
            var weapon = weapons[weaponNumber]
            projectiles = weapon.getProjectile()
        }
        return projectiles
    }
}