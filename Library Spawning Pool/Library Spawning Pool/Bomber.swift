//
//  Bomber.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class Bomber : ShipProtocol
{
    var name : String
    var weapons : [WeaponProtocol]
    var sprite : SKSpriteNode
    var VelocityMultiplier : Double
    init()
    {
        name = "Bomber"
        weapons = [LightBomb(), TripleLaser()]
        sprite = SKSpriteNode(imageNamed: "enemyBlack4.png")
        VelocityMultiplier = 0.75
    }
    
    func AttemptToFire(weaponNumber : Int) -> [ProjectileProtocol]
    {
        var projectiles : [ProjectileProtocol] = []
        
        if(weaponNumber < 0)
        {
            return projectiles
        }
        if weaponNumber < weapons.count
        {
            var weapon = weapons[weaponNumber]
            projectiles = weapon.getProjectile()
        }
        else
        {
            projectiles = AttemptToFire(weaponNumber - 1)
        }
        return projectiles
    }
}