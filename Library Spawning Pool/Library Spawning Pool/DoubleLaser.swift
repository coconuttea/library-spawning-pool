//
//  DoubleLaser.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
class DoubleLaser : WeaponProtocol {
    var Name : String
    var Damage : Double
    var Speed : Double
    var Offset : [CGPoint]
    var VelocityOffset : [CGPoint]
    init()
    {
        Name = "Double Laser"
        Damage = 15.0
        Speed = 2.0
        Offset = [CGPoint(x: -20.0, y: 0.0), CGPoint(x: 20.0, y: 0.0)]
        VelocityOffset = [CGPoint(x: 0.0, y:0.0), CGPoint(x: 0.0, y: 0.0)]
    }
    
    func getProjectile() -> [ProjectileProtocol] {
        
        var projArray : [ProjectileProtocol] = []
        
        for i in 0..<2
        {
//            var sprite = SKSpriteNode(imageNamed: SpriteImage)
            var projectile = DoubleLaserProjectile(offset: Offset[i], VelocityOffset: VelocityOffset[i])
            projArray.append(projectile)
            
        }

        return projArray
    }
}