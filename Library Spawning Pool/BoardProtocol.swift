//
//  BoardProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/9/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol BoardProtocol {
    var Players : [PlayerProtocol] {get set }
}