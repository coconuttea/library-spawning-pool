//
//  TripleLaser.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/2/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
class TripleLaser : WeaponProtocol, VehicalLibraryBaseProtocol {
    var Name : String
    var Ammo : Int = 100
    var Speed : Double
    var Offset : [CGPoint]
    var VelocityOffset : [CGPoint]
    
    init() {
        Name = "Triple Laser"
        Speed = 2.0

        Offset = [CGPoint(x: 0.0, y: 0.0), CGPoint(x: 10.0, y: 0.0), CGPoint(x: -10.0, y: 0.0)]
        VelocityOffset = [CGPoint(x: 0.0, y:0.0), CGPoint(x: 200.0, y: 0.0), CGPoint(x: -200.0, y: 0.0)]
    }
    func getProjectile() -> [ProjectileProtocol] {
        var projectiles : [ProjectileProtocol] = []
        for i in 0..<3
        {
            var projectile = TripleLaserProjectile(offset: Offset[i], VelocityOffset: VelocityOffset[i])
            projectiles.append(projectile)
            projectile.Sprite.runAction(SKAction.repeatActionForever(SKAction.rotateByAngle(3.14159, duration: 0.1)))
            
        }
        
        return projectiles
    }
    
    func getNewInstance() -> VehicalLibraryBaseProtocol {
        return TripleLaser()
    }
}