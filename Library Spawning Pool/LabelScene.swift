//
//  LabelScene.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/5/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
class LabelScene : SKScene
{
    var label : SKLabelNode = SKLabelNode(text: "Hello")
    let leftButton = SKSpriteNode(imageNamed: "arrowLeft.png")
    let rightButton = SKSpriteNode(imageNamed: "arrowRight.png")
    let downButton = SKSpriteNode(imageNamed: "arrowDown.png")
    let upButton = SKSpriteNode(imageNamed: "arrowUp.png")
    var currentLetter : String = "A"
    var infoLabel : SKLabelNode = SKLabelNode(text: "Num Chars: ")
    
    var index = 0
    var AtoZ = [
        "A", // 17
        "B", // 16
        "C", // 14
        "D", // 15
        "E", // 18
        "F", // 20
        "G", // 13
        "H", // 15
        "I", // 90?
        "J", // 21
        "K", // 16
        "L", // 20
        "M", // 12
        "N", // 15
        "O", // 13
        "P", // 17
        "Q", // 13
        "R", // 16
        "S", // 16
        "T", // 19
        "U", // 15
        "V", // 16
        "W", // 11
        "X", // 18
        "Y", // 17
        "Z", // 19
        " ", // 33
        ".", // 33
        ",", // 33
        ":", // 33
        "!"  // 50
        
    ]
    override func didMoveToView(view: SKView) {

        initTextTester()
    }
    
    func initTextTester()
    {
        label = SKLabelNode(text: currentLetter)
        label.position = CGPoint(x: 0.0, y: size.height / 2 )
        label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        self.addChild(label)
        
        leftButton.position.x = size.width * 5.5 / 8
        leftButton.position.y = size.height / 8
        
        rightButton.position.x = size.width * 7 / 8
        rightButton.position.y = size.height / 8
        
        upButton.position.x = size.width  / 8
        upButton.position.y = size.height / 5.5
        
        downButton.position.x = (size.width) / 8
        downButton.position.y = size.height / 10.5
        
        self.addChild(leftButton)
        self.addChild(rightButton)
        self.addChild(downButton)
        self.addChild(upButton)

    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>)
        {
//            label.position = touch.locationInNode(self)
            
//            var bub = TextBoxWidthTestingBubble()
//            bub.text = "HELLO"
//            bub.runActions()
//            bub.position = touch.locationInNode(self)
            
//            self.addChild(bub)
            
            let x = nodeAtPoint(touch.locationInNode(self))
            
            if(x == leftButton)
            {
                leftClicked()
            }
            
            if(x == rightButton)
            {
                rightClicked()
            }
            if(x == downButton)
            {
                downClicked()
            }
            if(x == upButton)
            {
                upClicked()
            }
        }
    }
    
    func leftClicked() {
        println("Left")
    }
    
    func rightClicked() {
        println("Right")
        index++
        let newChar = AtoZ[index]
        
        label.removeFromParent()
        
        updateLabel(newChar)
        currentLetter = newChar
        self.addChild(label)
        updateInfoLabel()
    }
    
    func upClicked() {
        println("Up")
        var labelText = label.text
        labelText += currentLetter
        
        updateLabel(labelText)
        updateInfoLabel()
        self.addChild(label)
    }
    
    func downClicked() {
        println("Down")
        var labelText = label.text
        if(count(labelText) > 0)
        {
            labelText = dropLast(labelText)
            updateLabel(labelText)
            updateInfoLabel()
            self.addChild(label)
        }
    }
    
    func updateInfoLabel()
    {
        infoLabel.removeFromParent()
        var textCount = count(label.text)
        var str = String(format: "Num Chars: %d", textCount)
        //        println(str)
        infoLabel = SKLabelNode(text: str)
        
        infoLabel.position.x = size.width / 2
        infoLabel.position.y = size.height * 7 / 8
        
        self.addChild(infoLabel)
    }
    
    func updateLabel(text : String)
    {
        
        label.removeFromParent()
        label = SKLabelNode(text: text)
        label.position = CGPoint(x: 0.0, y: size.height / 2 )
        label.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
    }
    
}