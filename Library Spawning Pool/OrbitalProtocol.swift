//
//  OrbitalProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/6/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol OrbitalProtocol {
    var Sprite : SKSpriteNode { get set }
    var Weapons : [WeaponProtocol] { get set }
    var OrbitRadius : Double { get set }
    var Base : SKNode { get set }
    func Attack() -> [ProjectileProtocol]
}