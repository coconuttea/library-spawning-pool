//
//  FriendWebServiceConnector.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/13/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

class FriendWebServiceConnector : NSURLConnection, NSURLConnectionDataDelegate {
    var downloadedData : NSMutableData = NSMutableData()
    var friends = NSMutableArray();
    
    func Connect() {
        newConnect();
    }
    
    func newConnect() {

        var postString = ""
        for i in 0..<6 {
            if(postString == "") {
                postString = "id=\(i)";
            }
            else {
                postString = "\(postString),\(i)";
            }
        }
        
        println("poststring: \(postString)")

        let body = postString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true)
        
        let url = NSURL(string: "http://jonathanwong.local/friends.php");
        
        var request = NSMutableURLRequest(URL: url!)
        
        var connection = NSURLConnection(request: request, delegate: self, startImmediately: true);
        
        request.HTTPMethod = "POST";
        request.HTTPBody = body;
        
        var session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request, completionHandler: {
            data, response, error -> Void in
            var d = NSString(data: data, encoding: NSUTF8StringEncoding)
//            self.friends = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves, error: nil) as! NSMutableArray
//            
//            self.PrintFriends()
            
            
            if let friends = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves, error: nil) as? NSMutableArray {
                println("it's an array");
            }
            
            if let friends = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableLeaves, error: nil) as? NSMutableDictionary {
                println("It's a dict");
            }
            
//            println("data: \(d!)")
        })
        
        task.resume()
    }
    
    func PrintFriends() {
        for friend in friends {
            if let name = friend.objectForKey("name") as? NSString {
                println("friend information: \(name)")
            }

        }
    }
    
    func oldConnect() {
        let id = 1
        var con = NSURL(string: "http://jonathanwong.local/friends.php");
        var request = NSMutableURLRequest(URL: con!);
        
        var connection = NSURLConnection(request: request, delegate: self, startImmediately: true)
        
        request.HTTPShouldHandleCookies = false;
        
        request.HTTPMethod = "GET"
        
        var err: NSError?
        
        //        var params = "id":"1" as Dictionary<String, String>
        
        var params = Dictionary<String, String>()
        
        
        params.updateValue("1", forKey: "id");
        
        let body = NSJSONSerialization.dataWithJSONObject(params, options: nil, error: &err)
        request.HTTPBody = body
        
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        
        
        var response: AutoreleasingUnsafeMutablePointer<NSURLResponse?>=nil
        //        var err : NSError
        
        connection!.start()
        println(request);
        
        NSURLConnection.sendSynchronousRequest(request, returningResponse: response, error: &err)
        
        
        var session = NSURLSession.sharedSession()
        let task = session.dataTaskWithRequest(request, completionHandler: {
            data, response, error -> Void in
            println("\(response)")
        })
        
        task.resume()
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        println("did finish loading");
    }
    
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        println("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        println(data.description);
        println(data)
        downloadedData.appendData(data)
        var error : NSError
        
                var json : NSMutableArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSMutableArray
//        var json : NSMutableDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSMutableDictionary;
        
        //        for value in json {
        //            println("\(value.key) : \(value.value)");
        //        }
    }
    
    //    func connectionDidFinishLoading(connection: NSURLConnection!) {
    //        println("finished loading")
    //    }
    

}