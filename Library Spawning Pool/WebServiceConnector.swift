//
//  WebServiceConnector.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/12/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

class WebServiceConnector : NSURLConnection, NSURLConnectionDataDelegate {
    
    var downloadedData : NSMutableData = NSMutableData()
    func Connect() {
        var con = NSURL(string: "http://jonathanwong.local/redeem.php");
//        var con = NSURL(string: "http://jonathanwong.local/friends.php");

        var urlRequest = NSMutableURLRequest(URL: con!);
        
//        var connection = NSURLConnection(request: urlRequest, delegate: self);
        var connection = NSURLConnection(request: urlRequest, delegate: self, startImmediately: true)
        
        urlRequest.HTTPShouldHandleCookies = false;
        urlRequest.HTTPMethod = "POST"
        let getString = "id=1";
//        let data = getString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: false)
        let data = getString.dataUsingEncoding(NSUTF8StringEncoding, allowLossyConversion: true);
        urlRequest.HTTPBody = data;
        
        
        var response: AutoreleasingUnsafeMutablePointer<NSURLResponse?>=nil
//        var err : NSError
        
        connection!.start()

        
        NSURLConnection.sendSynchronousRequest(urlRequest, returningResponse: response, error: nil)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        println("did finish loading");
    }
    
    func connection(didReceiveResponse: NSURLConnection!, didReceiveResponse response: NSURLResponse!) {
        println("didReceiveResponse")
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        println(data.description);
        println(data)
        downloadedData.appendData(data)
        var error : NSError
        
//        var json : NSMutableArray = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSMutableArray
        var json : NSMutableDictionary = NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.MutableContainers, error: nil) as! NSMutableDictionary;
        
//        for value in json {
//            println("\(value.key) : \(value.value)");
//        }
    }
    
//    func connectionDidFinishLoading(connection: NSURLConnection!) {
//        println("finished loading")
//    }
    
    
    
}
