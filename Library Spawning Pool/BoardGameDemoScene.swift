//
//  BoardGameDemoScene.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/9/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class BoardGameDemoScene : SKScene {
    
    var Board : BoardGameBoard = BoardGameBoard()
    var locationDictionary = Dictionary<String, SKNode>()
    let locations = ["Hand", "Graveyard", "Board"]
            var player = BoardGamePlayer()
    var handNode : CGPoint = CGPoint()
    
    var maxWidth : CGFloat = 0.0;
    
    var selectedCard : CardProtocol? = nil;
    
    override func didMoveToView(view: SKView) {
        initializeGame()
        handNode = CGPoint(x: self.size.width * 0.33, y: self.size.height * 0.20);
    }
    
    func initializeGame() {
        for location in locations {
            var node = SKNode()
            node.addChild(SKSpriteNode(imageNamed: "cardBack_blue1.png"))
            locationDictionary.updateValue(node, forKey: location)
        }

        locationDictionary["Hand"]!.position.x = self.size.width *  0.33
        locationDictionary["Hand"]!.position.y = self.size.height * 0.20
        
//        locationDictionary["Hand"]!.physicsBody = SKPhysicsBody()
        
        self.addChild(locationDictionary["Hand"]!)
        
        locationDictionary["Graveyard"]!.position.x = self.size.width * 0.90
        locationDictionary["Graveyard"]!.position.y = self.size.height * 0.20
        self.addChild(locationDictionary["Graveyard"]!)
        
        backgroundColor = UIColor(red: 0.4, green: 0.4, blue: 0.4, alpha: 1.0)
        

        player.Name = "Jonathan"
        player.HitPoints = 30
        
//        player.Library.Shuffle();

        player.Hand = player.Library.Draw(1);
        maxWidth = player.Hand[0].CardSprite.size.width * 3.5
        Board.Players.append(player)
        
        var i = 1;
        for card in player.Hand {
            println(card.Name)
            
            card.BaseNode.position = CGPoint(x: card.CardSprite.size.width * CGFloat(i), y: card.CardSprite.size.height * CGFloat(1));
            self.addChild(card.BaseNode);
            i++;
        }
        
//        fetchPlayersFromServer();
//        fetchFriends();
//        runTestService();
        
//        fetchUsers();
//        runTruckService()
        
        
    }
    
    func runTruckService() {
        let ws = TruckWebServiceConnector()
        ws.Connect()
    }
    
    func fetchUsers() {
        let ws = UsersWebServiceConnector();
        
        ws.Connect();
    }
    
    func runTestService() {
        let webService = TestServiceConnector()
        webService.Connect()
    }
    
    func fetchPlayersFromServer() {
        var webService = WebServiceConnector()
        
        webService.Connect()
        
    }
    
    
    func fetchFriends() {
        var friendService = FriendWebServiceConnector();
        
        friendService.Connect()
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let node = nodeAtPoint(touch.locationInNode(self)) as SKNode
            
            for card in Board.Players[0].Hand {
                if(card.CardSprite == node as? SKSpriteNode) {
                    println("Contact \(card.Name)");
                    selectedCard = card;
                }
            }
            
            for key in locationDictionary.keys {
                if node.parent == locationDictionary[key] {
                    println("\(key) touched!")
//                    node.addChild(SKSpriteNode(imageNamed:"cardBack_green1.png"))
                    
                    if(key == "Graveyard") {
                        drawCard();
                    }
                    
                }
            }
        }
    }
    
    func drawCard() {
//        player.Hand.append(player.Library.Draw(1)[0]);
        
        if(player.Library.Cards.count == 0) {
            
            for card in player.Hand {
                card.BaseNode.removeFromParent()
            }
        
            player.Library.Cards = player.Hand
            player.Hand = [CardProtocol]();
            
        }
        
        let cardsToDraw = player.Library.Draw(1);
        
        for card in cardsToDraw {
            player.Hand.append(card);
        }
        for card in player.Hand {
            card.BaseNode.removeFromParent()
        }
        
        sortPositions(player.Hand);
        
        for i in 0..<player.Hand.count {
            self.addChild(player.Hand[i].BaseNode);
        }
        
        
    }
    
    func sortPositions(hand : [CardProtocol]) {
        
        let middle = locationDictionary["Hand"]!.position.x;
        
        if(player.Hand.count < 54) {
            if(player.Hand.count % 2 == 0) {
                let firstIndex = player.Hand.count / 2 - 1
                let secondIndex = player.Hand.count / 2
                
                player.Hand[firstIndex].BaseNode.position.x = middle - player.Hand[firstIndex].CardSprite.size.width / 2;
                player.Hand[firstIndex].BaseNode.position.y = self.size.height * 0.25;
                player.Hand[secondIndex].BaseNode.position.x = middle + player.Hand[secondIndex].CardSprite.size.width / 2;
                player.Hand[secondIndex].BaseNode.position.y = self.size.height * 0.25;
            }
            
            if(player.Hand.count % 2 == 1) {
                let middleIndex = player.Hand.count / 2 - 1
                player.Hand[middleIndex].BaseNode.position.x = middle;
                let offset = player.Hand[middleIndex].CardSprite.size.width / 3;
                for i in 0..<middleIndex {
                    let offsetFromMiddle = CGFloat(middleIndex - i) * offset;
                    player.Hand[i].BaseNode.position.x = player.Hand[middleIndex].BaseNode.position.x - offsetFromMiddle
                }
                for i in (middleIndex + 1)..<player.Hand.count {
                    let offsetFromMiddle = CGFloat(i) * offset;
                    player.Hand[i].BaseNode.position.x = player.Hand[middleIndex].BaseNode.position.x + offsetFromMiddle
                    player.Hand[i].BaseNode.position.y = self.size.height * 0.25;
                }
            }
        }
        
        return;
        //we have max width, we have card count
        
//        let middle = maxWidth / 2.0;

        let maxOccupiedSpace = maxWidth / CGFloat(hand.count);
        if(hand.count % 2 == 1){
            hand[hand.count / 2 + 1].BaseNode.position.x = middle;
            println("Middle card is \(hand[hand.count / 2 + 1].Name)")
            
            for i in 0...(hand.count / 2) {
                hand[i].BaseNode.position.x = CGFloat(i) * maxOccupiedSpace;
                hand[i].BaseNode.position.y = hand[i].CardSprite.size.height;
            }
            
            for i in (hand.count / 2 + 2)..<hand.count {
                hand[i].BaseNode.position.x = CGFloat(i) * maxOccupiedSpace;
                hand[i].BaseNode.position.y = hand[i].CardSprite.size.height * 2;
            }
            
            
        }
        
//        if(hand.count > 0) {
//            for i in 0..<hand.count  {
//                
//                
//                hand[i].BaseNode.position.x = CGFloat(50 * i);
//                hand[i].BaseNode.position.y = hand[i].CardSprite.size.height * CGFloat(1)
//            }
//        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if(selectedCard != nil) {
            selectedCard = nil;
        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {

        for touch in (touches as! Set<UITouch>) {
            if(selectedCard != nil) {
//                selectedCard!.BaseNode.position = touch.locationInNode(self)
                selectedCard!.BaseNode.runAction(SKAction.moveTo(touch.locationInNode(self), duration: 0.1))
            }
            
        }
    }
    
}
