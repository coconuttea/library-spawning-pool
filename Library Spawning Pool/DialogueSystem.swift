//
//  DialogueSystem.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/4/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

class DialogueSystem {
    
    func getDialogue(c: CharacterProtocol) -> String {
        //dialogueEvaluator.getRelevantDialogue(Character, Setting, etc...)
        
        if(c.Name == "Hero") {
            return "Dialogue for Hero"
    
        }
        return "NO DIALOGUE SET"
    }
}