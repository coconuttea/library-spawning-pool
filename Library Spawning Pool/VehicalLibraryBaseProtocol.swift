//
//  VehicalLibraryBaseProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/14/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

protocol VehicalLibraryBaseProtocol {
    func getNewInstance() -> VehicalLibraryBaseProtocol
}