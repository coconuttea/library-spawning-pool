//
//  MultilineBubble.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/4/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class MultilineBubble : SKNode, BubbleProtocol {
    var text : String
    var duration : Double
    var sprites : [SKNode]
    var Node : SKNode = SKNode()
    override init()
    {
        text = ""
        duration = 10.0
        sprites = []
        super.init()
        
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func runActions()
    {
        parseText()
        
        let wait = SKAction.waitForDuration(20.0)
        let remove = SKAction.runBlock({self.removeFromParent()})
        let seq = SKAction.sequence([wait, remove])
        
        self.runAction(seq)
    }
    
    func parseText()
    {
        var maxChar = 22;
        
        var stringArray = split(text) { $0 == " " }
        
        var testLine = ""
        var lineNum = 0
        for s in stringArray {
            if(count(testLine + s) > maxChar)
            {
                let curLabel = SKLabelNode(text: testLine);
                self.addChild(curLabel)
                curLabel.position.y = CGFloat(lineNum) * -30.0
                curLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
                testLine = s + " "
                lineNum++
            }
            else
            {
                testLine += s + " "
            }
        }
        
        
        
    }
}