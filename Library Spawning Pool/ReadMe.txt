Setting is a representation of a location such as the world, a town, a building

Character - self exp

FloorTile - representation of a 'background'

InanimateObject - representation of an object living within the scope of some Setting, can be interactive

SideEffect - self exp, can be applied to characters via FloorTiles, Settings, etc.