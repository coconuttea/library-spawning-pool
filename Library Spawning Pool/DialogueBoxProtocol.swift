//
//  DialogueBoxProtocol.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/4/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol DialogueBoxProtocol {
    var textLabel : [SKLabelNode] { get set }
    var textLabelBackground : SKSpriteNode { get set }

}