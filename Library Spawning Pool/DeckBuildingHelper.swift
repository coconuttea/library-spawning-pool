//
//  DeckBuildingHelper.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/23/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
class DeckBuildingHelper {
    static func Get52Cards() -> [CardProtocol] {
        var Cards : [CardProtocol] = []
        let suits : [String] = ["Hearts", "Clubs", "Diamonds", "Spades"]
        let values : [String] = ["A", "2", "3", "4", "5", "6", "7", "8", "9", "10", "J", "Q", "K"]
        var dict = Dictionary<String, String>()
        
        dict.updateValue("Ace", forKey: "A")
        dict.updateValue("Jack", forKey: "J")
        dict.updateValue("Queen", forKey: "Q")
        dict.updateValue("King", forKey: "K")
        
        var valueDict : Dictionary<String , Int>
        valueDict = Dictionary<String, Int>()
        
        valueDict.updateValue(1, forKey: "Ace")
        valueDict.updateValue(10, forKey: "Jack")
        valueDict.updateValue(10, forKey: "Queen")
        valueDict.updateValue(10, forKey: "King")
        
        
        for suit in suits {
            for value in values {
                let imgString = "card\(suit)\(value).png"
             
                var card = PlayingCard()
                
                if valueDict[value] != nil
                {
                    card.Value = "\(valueDict[value]!)";
                }
                card.Type = suit
                var cardValue = value;
                if dict.indexForKey(value) != nil {
                    cardValue = dict[value]!;
                }
                
                card.Name = "\(cardValue) of \(suit)"
                card.BaseNode = SKNode()
                
                println(card.Name)
                
                card.CardSprite = SKSpriteNode(imageNamed: imgString)
                
                card.BaseNode.addChild(card.CardSprite)
                
                Cards.append(card)
            }
        }
        
        
        return Cards;
    }
}