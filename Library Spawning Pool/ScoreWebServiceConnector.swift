//
//  ScoreWebServiceConnector.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/20/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

class ScoreWebServiceConnector : NSURLConnection, NSURLConnectionDataDelegate {
    func Connect() {
        var con = NSURL(string: "http://jonathanwong.local/scores.php");
        var request = NSMutableURLRequest(URL: con!)
        request.HTTPMethod = "GET"
        var err: NSError?
        
        var response: AutoreleasingUnsafeMutablePointer<NSURLResponse?> = nil
        
        var connection = NSURLConnection(request: request, delegate: self, startImmediately: true);
    }
}