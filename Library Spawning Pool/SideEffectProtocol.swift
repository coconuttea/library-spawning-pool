//
//  SideEffectProtocol.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/3/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

protocol SideEffectProtocol {
    func applySideEffect(Character : CharacterProtocol)
}