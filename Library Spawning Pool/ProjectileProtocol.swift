//
//  ProjectileProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol ProjectileProtocol {
    var BaseNode : SKNode { get set }
    var Sprite : SKSpriteNode { get set }
    var Damage : Double { get set }
    var Speed : Double { get set }
    var Crit : Bool { get set }
    var CritChance : Double { get set }
    var CritModifier : Double { get set } 
    var Offset : CGPoint { get set }
    var VelocityOffset : CGPoint { get set }
}