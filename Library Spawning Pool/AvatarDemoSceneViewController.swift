//
//  AvatarDemoSceneViewController.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/12/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import UIKit
import SpriteKit

extension SKNode {
    class func unarchiveAvatarSceneFromFile(file : String) -> SKNode? {
        let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks")
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! AvatarDemoScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}
class AvatarDemoSceneViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let scene = AvatarDemoScene.unarchiveAvatarSceneFromFile("AvatarDemoScene") as? AvatarDemoScene {
            // Configure the view.
            let skView = self.view as! SKView
            skView.showsFPS = true
            skView.showsNodeCount = true
            
            /* Sprite Kit applies additional optimizations to improve rendering performance */
            skView.ignoresSiblingOrder = true
            
            /* Set the scale mode to scale to fit the window */
            scene.scaleMode = .AspectFill
            
            skView.presentScene(scene)
        }

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    override func prefersStatusBarHidden() -> Bool {
        return true
    }
}
