//
//  BubbleProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/4/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol BubbleProtocol
{
    var text : String { get set }
    var duration : Double { get set }
    var sprites : [SKNode] { get set }
    var Node : SKNode { get set }
    func runActions()
}
