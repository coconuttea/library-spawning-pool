//
//  CardScene.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/23/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class CardScene : SKScene {
    var Deck : DeckProtocol = StandardPlayingCardDeck()
    var Hand : [CardProtocol] = []
    var SelectedCard : CardProtocol = PlayingCard()
    var numCardsToDraw = 1
    
    
    var Players : [PlayerProtocol] = []
    override func didMoveToView(view: SKView) {
        
        initGame()
        
        backgroundColor = UIColor(red: 0.08, green: 0.65, blue: 0.08, alpha: 1.0)
    }
    
    func initGame() {
        initDeck()
        initPlayers();
    }
    
    func initDeck() {
        Deck.Shuffle()
    }
    
    func initPlayers() {
        var player1 = PokerPlayer();
        displayHand(player1.Hand);
        Players.append(player1);
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>) {
            let selected = nodeAtPoint(touch.locationInNode(self))
            
            for card in Hand {
                if card.BaseNode == selected.parent {
                    println("\(card.Name) found")
                    SelectedCard = card
                    return
                }
            }
            
        }
        
        
        let cards = Deck.Draw(numCardsToDraw)
        for card in cards {
//            card.BaseNode.position = CGPoint(x: size.width / 2, y: 0)
//            self.addChild(card.BaseNode)
//            card.BaseNode.runAction(SKAction.moveToY( size.height / 2, duration: 0.2))
            card.BaseNode.position.x = size.width;
            card.BaseNode.position.y = size.height / 2
            
            Hand.append(card)
            card.Flip()
            self.addChild(card.BaseNode)
        }
        
        organizeHand()
//        
//        if(cards.count > 0)
//        {
//            cards[0].BaseNode.position.x = size.width / 2
//            cards[0].BaseNode.position.y = size.width / 2
//            self.addChild( cards[0].BaseNode)
//        }
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {

            var index = 0
            
            SelectedCard.BaseNode.position = touch.locationInNode(self)
            SelectedCard.BaseNode.zPosition = CGFloat.max
//            for i in 0..<Hand.count {
//                if(SelectedCard == Hand[i])
//                {
//                    index = i
//                }
//            }
//            
//            Hand.removeAtIndex(index)
        }
        
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        if SelectedCard.Name != "" {
            SelectedCard.BaseNode.zPosition = CGFloat.max - 1
                                SelectedCard.Flip()
            if SelectedCard.BaseNode.position.y > size.height / 3
            {
                var position = -1;
                for i in 0..<Hand.count{
                    if Hand[i].BaseNode == SelectedCard.BaseNode {
                        position = i
                        break
                    }
                }
                
                if(position >= 0)
                {
                    Hand.removeAtIndex(position)
                    organizeHand();
                    
                }
            }
            SelectedCard = PlayingCard()
        }
    }
    
    func displayHand(hand : [CardProtocol])
    {
    
    }
    
    func organizeHand() {
        let count = Hand.count;
        let gap = size.width / CGFloat(count)
        for i in 0..<count {
            let card = Hand[i]
//            card.BaseNode.position.x = 0.0 + CGFloat(i + 1) * gap
//            card.BaseNode.position.y = size.height * 0.20
            
            let moveTo = CGPoint(x: 0.0 + CGFloat(i + 1) * gap, y: size.height * 0.20)
            let moveAction = SKAction.moveTo(moveTo, duration: 0.500);
            let block = SKAction.runBlock({
                if card.FaceDown {
                    card.Flip()
                }
            })
            
            let seq = SKAction.sequence([moveAction, block])
            card.BaseNode.runAction(seq)
            
            
            
            
            card.BaseNode.zPosition = CGFloat(i)
        }
    }
}