//
//  Corsair.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
class Corsair : ShipProtocol {
    var ShipName : String
    var weapons : [WeaponProtocol]
    var BaseNode : SKNode = SKNode()
    var sprite : SKSpriteNode
    var VelocityMultiplier : Double
    var Orbitals : [OrbitalProtocol] = []
    init()
    {
        ShipName = "Corsair"
        weapons = [SingleLaser(), DoubleLaser()]
        sprite = SKSpriteNode(imageNamed:"enemyBlack3.png")
        BaseNode.addChild(sprite)
        VelocityMultiplier = 1.5
        
    }

    func AttemptToFire(weaponNumber : Int) -> [ProjectileProtocol] {
        var projectiles : [ProjectileProtocol] = []
        if weaponNumber < weapons.count
        {
            var weapon = weapons[weaponNumber]
            projectiles = weapon.getProjectile()
            if projectiles.count == 0
            {
                let ammoAlert = OneLineMessageBubble(text: "Out of Ammo!")
                
                ammoAlert.runActions()
                ammoAlert.Node.zRotation = 3.141592
                
                self.sprite.addChild(ammoAlert.Node)
                
            }
        }
        
        
        return projectiles
    }
}