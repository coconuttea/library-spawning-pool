//
//  SettingProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/2/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol SettingProtocol {
    var Settings : [SettingProtocol] { get set }
    var Characters : [CharacterProtocol] { get set }
    var FloorTiles : [FloorTileProtocol] { get set }
    
    var Name : String { get set }
}