//
//  ControllerProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 11/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

protocol ControllerProtocol {
    var ControllerName : String { get set }
}