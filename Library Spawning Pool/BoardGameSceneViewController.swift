//
//  BoardGameSceneViewController.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/9/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import UIKit
import SpriteKit

extension SKNode {
    class func unarchiveBoardGameLibraryFromFile(file : String) -> SKNode? {
//        let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks")
        if let path = NSBundle.mainBundle().pathForResource(file, ofType: "sks") {
            var sceneData = NSData(contentsOfFile: path, options: .DataReadingMappedIfSafe, error: nil)!
            var archiver = NSKeyedUnarchiver(forReadingWithData: sceneData)
            
            archiver.setClass(self.classForKeyedUnarchiver(), forClassName: "SKScene")
            let scene = archiver.decodeObjectForKey(NSKeyedArchiveRootObjectKey) as! BoardGameDemoScene
            archiver.finishDecoding()
            return scene
        } else {
            return nil
        }
    }
}

class BoardGameSceneViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        if let scene = BoardGameDemoScene.unarchiveBoardGameLibraryFromFile("BoardGameDemoScene") as? BoardGameDemoScene {
            let skView = self.view as! SKView
            
            scene.scaleMode = .AspectFill
            
            skView.presentScene(scene)
        }
        

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
