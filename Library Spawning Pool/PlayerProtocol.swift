//
//  PlayerProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol PlayerProtocol {
    var Name : String { get set }
    var Board : [CardProtocol] { get set }
    var Hand : [CardProtocol] { get set }
    var Library : DeckProtocol { get set }
    var Graveyard : [CardProtocol] { get set }
}