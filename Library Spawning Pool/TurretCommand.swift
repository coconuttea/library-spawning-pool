//
//  TurretCommand.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/20/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class TurretCommand : SKScene, SKPhysicsContactDelegate {
    var fireTouch : UITouch = UITouch()
    let weapon : WeaponProtocol = MachineGun()
    var ammoLabel : SKLabelNode = SKLabelNode()
    var currentAmmo : Int = 900;
    override func didMoveToView(view: SKView) {
        backgroundColor = UIColor(red: 0.0, green: 0.5, blue: 0.0, alpha: 1.0)
        
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
//            simpleFire(touch)
            
//            complexFire(touch)
            
            let block = SKAction.runBlock(
                {
                    self.complexFire(touch)
                }
            )
            
            let delay = SKAction.waitForDuration(0.1)
            let seq = SKAction.sequence([block, delay])
            let repeat = SKAction.repeatActionForever(seq)
            
            fireTouch = touch
            self.runAction(repeat, withKey: "openFire")
        }
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            if touch == fireTouch {
                self.removeActionForKey("openFire")
            }
        }
    }
    
    func complexFire(touch : UITouch)
    {
        if(currentAmmo <= 0)
        {
            return;
        }
//        let dblLaser = weapon
        let projectiles = weapon.getProjectile()
        
        for projectile in projectiles {
            
            projectile.Sprite.position = projectile.Offset
            
            projectile.BaseNode.addChild(projectile.Sprite)
            
            projectile.Sprite.runAction(SKAction.moveBy(CGVector(dx: 0.0, dy: size.height * 2), duration: 1.0))
            projectile.BaseNode.position.x = size.width / 2
            projectile.BaseNode.position.y = size.height * 0.10
            
            var zRotation : CGFloat = 0.0
            let touchLoc = touch.locationInNode(self);
            if( touchLoc.x > size.width / 2)
            {
                let dxDy = CGPoint(x: touchLoc.x - projectile.BaseNode.position.x, y: touchLoc.y - projectile.BaseNode.position.y)
                zRotation = calculateAngle(dxDy, quadrant: 0)
            }
            
            if(touchLoc.x < size.width / 2 )
            {
                zRotation = calculateAngle(CGPoint(x: projectile.BaseNode.position.x - touchLoc.x, y: touchLoc.y - projectile.BaseNode.position.y), quadrant: 3)
            }
            
            projectile.BaseNode.zRotation = zRotation
            self.addChild(projectile.BaseNode)
            updateAmmo()
        }
    }
    
    func updateAmmo() {
        currentAmmo--
        ammoLabel.removeFromParent()
        ammoLabel = SKLabelNode(text : "\(currentAmmo)")
        ammoLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Right
        ammoLabel.position.x = size.width * 0.70
        ammoLabel.position.y = size.height / 2
        self.addChild(ammoLabel)
    }
    
    func simpleFire(touch : UITouch)
    {
        let laser = SingleLaserProjectile(offset: CGPointZero, VelocityOffset: CGPointZero)
        laser.BaseNode.addChild(laser.Sprite)
        
        laser.Sprite.runAction(SKAction.moveBy(CGVector(dx: 0.0, dy: size.height * 2), duration: 1.0))
        
        laser.BaseNode.position.x = size.width / 2
        laser.BaseNode.position.y = size.height * 0.10
        
        var zRotation : CGFloat = 0.0
        let touchLoc = touch.locationInNode(self);
        if( touchLoc.x > size.width / 2)
        {
            let dxDy = CGPoint(x: touchLoc.x - laser.BaseNode.position.x, y: touchLoc.y - laser.BaseNode.position.y)
            zRotation = calculateAngle(dxDy, quadrant: 0)
        }
        
        if(touchLoc.x < size.width / 2 )
        {
            zRotation = calculateAngle(CGPoint(x: laser.BaseNode.position.x - touchLoc.x, y: touchLoc.y - laser.BaseNode.position.y), quadrant: 3)
        }
        
        laser.BaseNode.zRotation = zRotation
        self.addChild(laser.BaseNode)
    }
    
    func calculateAngle(dxDy : CGPoint, quadrant : Int) -> CGFloat {
        
        var oppAdj : CGFloat = CGFloat(0.0);
        var coeff = 1;
        if(quadrant % 2 == 0)
        {
            oppAdj = CGFloat(dxDy.x / dxDy.y)
        }
        else
        {
            oppAdj = CGFloat(dxDy.y / dxDy.x)
            coeff = -1
        }
        
        //        oppAdj = CGFloat(dxDy.x / dxDy.y)
        if(oppAdj < 0.0)
        {
            oppAdj *= -1
        }
        
        
        let result = atan(oppAdj)
        
        println(String(format:"%f", result))
        
        let rotateTo = -result + CGFloat(CGFloat(coeff) * 1.570796 * CGFloat(quadrant))
        
        return rotateTo
        
    }

}