//
//  EnergyCore.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/6/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class EnergyCore : ShipProtocol {
    var ShipName : String
    var weapons : [WeaponProtocol]
    var BaseNode : SKNode = SKNode()
    var sprite : SKSpriteNode
    var VelocityMultiplier : Double
    var Orbitals : [OrbitalProtocol] = []
    init()
    {
        ShipName = "Energy Core"
        weapons = [MachineGun(), TripleLaser()]
        sprite = SKSpriteNode(imageNamed: "enemyBlack2.png")
        BaseNode.addChild(sprite)
        VelocityMultiplier = 1.0

        AddOrbs(3)
    }
    func AddOrbs(orbs : Int)
    {
        let numOrbs = Orbitals.count + orbs
        let zRotation : CGFloat = CGFloat(6.28314 / Double(numOrbs))
        
        for orb in Orbitals {
            orb.Base.removeAllChildren()
            orb.Base.removeFromParent()
        }
        Orbitals.removeAll(keepCapacity: true)
        
        if(numOrbs <= 0)
        {
            return;
        }
        
        for i in 0..<numOrbs
        {
            let energyBall = EnergyBall()
            energyBall.runActions()
            energyBall.Base.zRotation = zRotation * CGFloat(i)
            self.sprite.addChild(energyBall.Base)
            Orbitals.append(energyBall)
        }

    }
    
    func AttemptToFire(weaponNumber : Int) -> [ProjectileProtocol] {        
        var projectiles : [ProjectileProtocol] = []
        
        if(weaponNumber < 0)
        {
            return projectiles
        }
        if weaponNumber < weapons.count
        {
            var weapon = weapons[weaponNumber]
            projectiles = weapon.getProjectile()
        }
        else
        {
            projectiles = AttemptToFire(weaponNumber - 1)
        }
        return projectiles
    }
    
}