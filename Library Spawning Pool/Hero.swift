//
//  Hero.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/3/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class Hero : CharacterProtocol {
    var Name : String
    var Gender : String
    var Sprite : SKSpriteNode
    
    init(heroImage: String, position: CGPoint) {
        Name = "Hero"
        Gender = "Male"
        Sprite = SKSpriteNode(imageNamed:heroImage)
        Sprite.position = position
    }
    
    
    func interact() -> String {
        var dialogueSystem : DialogueSystem = DialogueSystem()
        var text = dialogueSystem.getDialogue(self)
        
        return text
        
    }
}