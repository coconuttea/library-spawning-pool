//
//  BoardGamePlayer.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 8/9/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation

class BoardGamePlayer : PlayerProtocol {
    var HitPoints : Int
    var Name : String
    var Board : [CardProtocol]
    var Hand : [CardProtocol]
    var Library : DeckProtocol
    var Graveyard : [CardProtocol]
    
    init() {
        Name = "Default"
        Board = []
        Hand = []
        Library = StandardPlayingCardDeck()
        Graveyard = []
        HitPoints = 30
    }
}