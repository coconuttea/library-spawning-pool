//
//  StandardPlayingCardDeck.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/23/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class StandardPlayingCardDeck : DeckProtocol {
    var Cards : [CardProtocol]
    
    init() {
        Cards = DeckBuildingHelper.Get52Cards()
        initCards()
    }
    
    func Shuffle() {
        for j in 0..<100
        {
            for i in 0..<Cards.count
            {
                let card = Cards.removeAtIndex(random() % (Cards.count))
                Cards.insert(card, atIndex: random() % (Cards.count - 1))
            }
        }
    }
    
    func Draw(numberToDraw : Int) -> [CardProtocol] {
        var cardsDrawn :[CardProtocol] = []
        
        if(Cards.count < numberToDraw)
        {
            return Draw(Cards.count)
        }
        
        for i in 0..<numberToDraw {
//            cardsDrawn.append(Cards[0])
            let card = Cards.removeAtIndex(0)
            cardsDrawn.append(card)
        }
        
        return cardsDrawn
    }
    
    func Peek() -> String {
        if(Cards.count > 0) {
            return "\(Cards[0].Name) is the next card"
        }
        return "No cards left";
    }
    func Insert(card : CardProtocol, position: Int) {
        
    }
    
    func initCards() {
        let cards = DeckBuildingHelper.Get52Cards()
    }
}