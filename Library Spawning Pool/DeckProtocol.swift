//
//  DeckProtocol.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/23/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

protocol DeckProtocol {
    var Cards : [CardProtocol] { get set }
    
    func Shuffle()
    func Draw(numberToDraw : Int) -> [CardProtocol]
    func Peek() -> String
    func Insert(card : CardProtocol, position : Int)
}