//
//  LightBombProjectile.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/1/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class LightBombProjectile : ProjectileProtocol {
    var BaseNode : SKNode
    var Sprite : SKSpriteNode
    var Damage : Double = 100.0
    var Speed : Double = 0.34
    var Crit : Bool = false
    var CritChance : Double = -1.0 // crit should be impossible
    var CritModifier : Double = 1.0 // but in case it does, it'll just get a 0% damage bonus
    var Offset : CGPoint
        var VelocityOffset : CGPoint
    init(offset : CGPoint, VelocityOffset : CGPoint)
    {
        BaseNode = SKNode()
        let SpriteImage = "laserRed08.png"
        Sprite = SKSpriteNode(imageNamed: SpriteImage)
        Offset = offset
        self.VelocityOffset = VelocityOffset

        if( (Double(random()) % 100) <= CritChance)
        {
            Crit = true
            Damage *= CritModifier
        }

    }
}