//
//  Joystick.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 11/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class Joystick : ControllerProtocol {
    var ControllerName : String = "Joystick"
    var BaseNode : SKNode = SKNode()
    let outerCircle : SKShapeNode
    let innerCircle : SKShapeNode
    init() {
        outerCircle = SKShapeNode(circleOfRadius: 45.0);
        innerCircle = SKShapeNode(circleOfRadius: 20.0);
        
        outerCircle.strokeColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        outerCircle.fillColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.2)
        
        innerCircle.strokeColor = UIColor(red: 1.0, green: 1.0, blue: 1.0, alpha: 0.2)
        innerCircle.fillColor = UIColor(red: 0.0, green: 0.0, blue: 0.75, alpha: 1.0)
        
        BaseNode.addChild(outerCircle);
        BaseNode.addChild(innerCircle);
        
    }


}

