//
//  QuestionBox.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/4/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class QuestionBox : DialogueBoxProtocol {
    var textLabel : [SKLabelNode]
    var textLabelBackground : SKSpriteNode
    
    //answers to question (textLabel
    var answers : [SKLabelNode]
    
    
    init(answersStrings : [String]) {
        textLabel = []
        textLabel.append(SKLabelNode(text: "What is my name"))
        textLabel[0].position = CGPoint(x: 500, y: 500)
        textLabelBackground = SKSpriteNode()
        answers = []
        var x = 500
        var y = 400
        for answer in answersStrings {
            var node = SKLabelNode(text: answer)
            node.position = CGPoint(x: x, y: y)
            y = y - 50
            answers.append(node)
            
        }
        
    }

    
    
}