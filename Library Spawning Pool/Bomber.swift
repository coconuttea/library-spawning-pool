//
//  Bomber.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class Bomber : ShipProtocol
{
    var ShipName : String
    var weapons : [WeaponProtocol]
    var BaseNode : SKNode = SKNode()
    var sprite : SKSpriteNode
    var Orbitals : [OrbitalProtocol] = []
    var VelocityMultiplier : Double
    init()
    {
        ShipName = "Bomber"
        weapons = [LightBomb()]
        sprite = SKSpriteNode(imageNamed: "enemyBlack4.png")
        BaseNode.addChild(sprite)
        VelocityMultiplier = 0.75
    }
    
    func AttemptToFire(weaponNumber : Int) -> [ProjectileProtocol]
    {
        var projectiles : [ProjectileProtocol] = []
        
        if(weaponNumber < 0)
        {
            return projectiles
        }
        if weaponNumber < weapons.count
        {
            var weapon = weapons[weaponNumber]
            projectiles = weapon.getProjectile()
        }
        else
        {
            projectiles = AttemptToFire(weaponNumber - 1)
        }
        return projectiles
    }
}