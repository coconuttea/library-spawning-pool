//
//  TielScene.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/3/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class TileScene : SKScene {
    let mineralTown = Town(name: "Mineral Town")
    
    override func didMoveToView(view: SKView) {
        backgroundColor = SKColor(red: 0.2, green: 0.75, blue: 0.2, alpha: 1.0)
        renderSetting(mineralTown)
    }
    
    //should be a SettingRenderer Helper Class
    func renderSetting(setting : SettingProtocol) {
        var background : SKSpriteNode = mineralTown.FloorTiles.first!.Sprite;
        background.size = size
        self.addChild(background)
        self.addChild(mineralTown.Characters.first!.Sprite)
        
        var questionBox = QuestionBox(answersStrings: ["Matt", "Hero", "Anti-Hero", "Samwise"])

        self.addChild(questionBox.textLabel[0])
        self.addChild(questionBox.answers[0])
        self.addChild(questionBox.answers[1])
        self.addChild(questionBox.answers[2])
        self.addChild(questionBox.answers[3])
    }
}