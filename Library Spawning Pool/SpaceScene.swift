
//
//  SpriteScene.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit
import CoreMotion

class SpaceScene : SKScene, SKPhysicsContactDelegate {
    struct PhysicsCategory {
        static let None      : UInt32 = 0
        static let All       : UInt32 = UInt32.max
        static let EnemyShip   : UInt32 = 0b1       // 1
        static let Projectile: UInt32 = 0b10      // 2
    }
    
//    TODO: Implement joystick
    var joystick : Joystick = Joystick();
    
    var projectiles : [ProjectileProtocol] = []
    var ships : [ShipProtocol] = [Corsair(), Bomber(), Orbiter(), EnergyCore()]
    var currentShip : ShipProtocol = Corsair()
    var shipIterator : Int = 0
    var text = ""
    
    var buttonA : SKSpriteNode = SKSpriteNode()
    var buttonB : SKSpriteNode = SKSpriteNode()
    
    var motionManager : CMMotionManager = CMMotionManager()
    var moveTouch : UITouch = UITouch()
    var fireTouch : UITouch = UITouch()
    var lastMove : CGPoint = CGPointZero
    let minRollDeviation : Double = 0.05
    var moveOrigin : CGPoint = CGPoint(x: 0.0, y:0.0)
    var attitudeOrigin : CMAttitude = CMAttitude()
    var previousRoll : Double = 0.0
    var previousPitch : Double = 1.0
    var shotCounter = 0
    var tank : ShipProtocol = Tank()
    override func didMoveToView(view: SKView) {
        physicsWorld.gravity = CGVectorMake(0, 0)
        physicsWorld.contactDelegate = self
        if(motionManager.deviceMotionAvailable)
        {
            print("Success")
            motionManager.startDeviceMotionUpdates()
        }
        else
        {
            print("Failure")
        }
        initializeGame()
        cycleShips()
        
        currentShip.weapons = [MachineGun().getNewInstance() as! WeaponProtocol]
        
//        let newShip = Corsair();
//        newShip.BaseNode.position.x = size.width / 2
//        newShip.BaseNode.position.y = size.height / 2
//        newShip.BaseNode.zRotation = 3.141592
//        self.addChild(newShip.BaseNode)
        
        tank.BaseNode.position = CGPoint(x: size.width / 2, y: size.height / 2)
        
        buttonA = SKSpriteNode(imageNamed: "buttonA.png")
        buttonB = SKSpriteNode(imageNamed: "buttonB.png")
        
        buttonA.position = CGPoint(x: size.width * 0.75, y: size.height * 0.10)
        
        buttonB.position = CGPoint(x: size.width * 0.90, y: size.height * 0.10)
        self.addChild(buttonA);
        self.addChild(buttonB);
//        self.addChild(tank.BaseNode)
    }
    
    
    
    override func motionBegan(motion: UIEventSubtype, withEvent event: UIEvent) {
        if motionManager.deviceMotionAvailable
        {
            let accelData =
            String(format: "roll: %f, pitch: %f, yaw: %f",
                motionManager.deviceMotion.attitude.roll,
                motionManager.deviceMotion.attitude.pitch,
                motionManager.deviceMotion.attitude.yaw)
            
//
//            String(format: "roll: %f, pitch: %f, yaw: %f",
//                attitudeOrigin.roll,
//                attitudeOrigin.pitch,
//                attitudeOrigin.yaw)
//            
            println(accelData)
        }
    }
    
    override func update(currentTime: NSTimeInterval) {
        for star in stars
        {
            if(star.position.y < 0.0)
            {
                star.position.y = size.height
                star.position.x = CGFloat(random() % Int(size.width))
                star.removeAllActions()
                
                star.runAction(SKAction.repeatActionForever(SKAction.moveBy(CGVector(dx: 0.0, dy: -size.height), duration: 20)))
            }
        }
        
        handleMovementByRoll()
//        handleMovementByPitch()
        
//        handleShipBounds()
    }
    
    func handleShipBounds() {
        if(currentShip.BaseNode.position.x < 0.0)
        {
            currentShip.BaseNode.removeActionForKey("horizontalMove")
            currentShip.BaseNode.position.x = 0.0
        }
        if(currentShip.BaseNode.position.x > size.width)
        {
            currentShip.BaseNode.removeActionForKey("horizontalMove")
            currentShip.BaseNode.position.x = size.width
        }
        
        if(currentShip.BaseNode.position.y < 0.0)
        {
            currentShip.BaseNode.removeActionForKey("verticalMove")
            currentShip.BaseNode.position.y = 0.0
        }
        
        if(currentShip.BaseNode.position.y > size.height)
        {
            currentShip.BaseNode.removeActionForKey("verticalMove")
            currentShip.BaseNode.position.y = size.height
        }
    }
    
    func handleMovementByPitch() {
        if(!motionManager.deviceMotionAvailable)
        {
            return;
        }
        
        if(!motionManager.deviceMotionActive)
        {
            return;
        }
        
        if let attitude = motionManager.deviceMotion?.attitude
        {
            let pitch = attitude.pitch
            if(pitch <= 0.10 && pitch >= -0.10)
            {
                currentShip.sprite.removeActionForKey("verticalMove")
                previousPitch = pitch
                return;
            }
            
            if(pitch <= previousPitch + 0.10 && pitch >= previousPitch - 0.10)
            {
                // maintain velocity
                return;
            }
            
            if(pitch > previousPitch + 0.10 || pitch < previousPitch - 0.10)
            {
                currentShip.sprite.removeActionForKey("verticalMove")
                currentShip.sprite.runAction(SKAction.repeatActionForever(
                    SKAction.moveBy(CGVector(dx:0.0, dy: 1000.0 * 0.0-pitch), duration: 1.0)), withKey: "verticalMove")
                
                previousPitch = pitch;
            }

            
        
        }
        
    }
    
    func handleMovementByRoll() {
        if(!motionManager.deviceMotionAvailable)
        {
            return;
        }
        
        if(!motionManager.deviceMotionActive)
        {
            return
        }
        
        if let attitude = motionManager.deviceMotion?.attitude
        {
            let roll = attitude.roll
            
            if(roll <= previousRoll + minRollDeviation && roll >= previousRoll - minRollDeviation)
            {
                // maintain Velocity
                return
            }
            
            if(roll > previousRoll + minRollDeviation || roll < previousRoll - minRollDeviation)
            {
                currentShip.sprite.removeActionForKey("horizontalMove")
                currentShip.sprite.runAction(SKAction.repeatActionForever(
                    SKAction.moveBy(CGVector(dx:3000.0 * roll, dy: 0.0), duration: 1.0)), withKey: "horizontalMove")
                
                previousRoll = roll;
            }
        }
        
    }
    
    var stars : [SKSpriteNode] = []
    func initializeGame()
    {
        backgroundColor = SKColor(red: 0.001, green: 0.001, blue: 0.08, alpha: 1.0)
        for i in 0..<30
        {
            var star = SKSpriteNode(imageNamed: "star1.png")
            
            star.position.x = CGFloat(random() % Int(size.width))
            star.position.y = CGFloat(random() % Int(size.height))
            stars.append(star)
            let scale = random() % 20
            let scaleDouble = CGFloat(CGFloat(scale) / 100.0) + 0.20
            star.setScale(scaleDouble)
            let zRotation = CGFloat(CGFloat(random() % 314159) / 100000.0)
            star.zRotation = zRotation
            self.addChild(star)
            
            var star2 = SKSpriteNode(imageNamed: "star1.png")
//            star2.position = star.position
            star2.setScale(scaleDouble)
            star2.zRotation = zRotation + 1.570796
            
            star.addChild(star2)
            star.runAction(SKAction.repeatActionForever(SKAction.moveBy(CGVector(dx: 0.0, dy: -size.height), duration: 20)))
        }
        
        for i in 0..<4 {
//            var enemy = SKSpriteNode(imageNamed: "enemyBlack2.png")
            
            let enemyShip = Corsair()
            let enemy = enemyShip.BaseNode;
            enemy.position.x = size.width / 5 * CGFloat(i + 1)
            enemy.position.y = size.height - 100.0
            
            self.addChild(enemy)
//            enemy.zRotation = 3.141592
            enemy.physicsBody = SKPhysicsBody(circleOfRadius: enemyShip.sprite.size.width / 2.1)
            enemy.physicsBody?.contactTestBitMask = PhysicsCategory.Projectile
            enemy.physicsBody?.collisionBitMask = PhysicsCategory.None
            enemy.physicsBody?.categoryBitMask = PhysicsCategory.EnemyShip
            enemy.physicsBody?.dynamic = true
        }
        
    }
    
    func cycleShips()
    {
        
        currentShip = ships[shipIterator]
        for ship in ships
        {
            ship.BaseNode.removeFromParent()
        }
        currentShip.BaseNode.removeFromParent()
        
        currentShip.BaseNode.zRotation = 3.141592
        currentShip.BaseNode.position.x = self.size.width * 0.5
        currentShip.BaseNode.position.y = self.size.height * 0.25
        self.addChild(currentShip.BaseNode)
//        currentShip.sprite.zRotation = 3.141592
//        currentShip.sprite.position.x = self.size.width * 0.5
//        currentShip.sprite.position.y = self.size.height * 0.25
        


        var label = SKLabelNode(text: text)
        
        
        
        if(shipIterator < ships.count - 1)
        {
            shipIterator++
        }
        else
        {
            shipIterator = 0
        }
//        currentShip.sprite.zPosition = 1
//        currentShip.sprite.physicsBody = SKPhysicsBody(rectangleOfSize: currentShip.sprite.size)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {
            let loc = touch.locationInNode(self)
            
            
            joystick.BaseNode.position = loc;
            joystick.innerCircle.position = CGPoint(x:0.0, y:0.0);
            self.addChild(joystick.BaseNode)
            
            
            
            handleTouchBetweenNodeAndLocation(tank.BaseNode, touch: touch)
            if let node = nodeAtPoint(loc) as? SKSpriteNode
            {
                if currentShip.sprite == node
                {
                    cycleShips()
                }
            }
            
            if touch.locationInNode(self).x < size.width / 2
            {
                handleMovement(touch)
            }
            else
            {
//                handleWeapons(touch)
                if(nodeAtPoint(loc) == buttonA)
                {
                    handleWeapons(touch)
                }
                
                if(nodeAtPoint(loc) == buttonB)
                {
                    handleWeapons(touch)
                }
            }
            
        }
    }
    
    func handleTouchBetweenNodeAndLocation(nodeToRotate : SKNode, touch : UITouch) {
        let loc = touch.locationInNode(self)
        let nodePos = nodeToRotate.position;
        
        
        println(String(format: "tank x: %f, y: %f ", nodeToRotate.position.x, nodeToRotate.position.y))
        println(String(format: "touch x: %f, y: %f", loc.x, loc.y))
        
        //take care of exacts
        
        if(Int(loc.y) == Int(nodePos.y)){
            if(loc.x > nodePos.x) {
                nodeToRotate.runAction(
                    SKAction.rotateToAngle(-1.570796, duration: 1))
            }
            if(loc.x < nodePos.x) {
                nodeToRotate.runAction(
                    SKAction.rotateToAngle(1.570796, duration: 1))
            }
        }
        
        if(Int(loc.x) == Int(nodePos.x)) {
            if(loc.y > nodePos.y)
            {
                nodeToRotate.runAction(
                    SKAction.rotateToAngle(-1.570796, duration: 1))
            }
            if(loc.y < nodePos.y)
            {
                nodeToRotate.runAction(
                    SKAction.rotateToAngle(1.570796, duration: 1))
            }
        }
        
        // quadrants 1 and 2
        if(Int(loc.x) > Int(nodePos.x)) {
            
            //quadrant 1
            if(Int(loc.y) > Int(nodePos.y)) {
                calculateAngle(CGPoint(x: Int(loc.x) - Int(nodePos.x), y: Int(loc.y) - Int(nodePos.y)), offset: 0)
            }
            
            //quadrant 2
            if(Int(loc.y) < Int(nodePos.y)) {
                calculateAngle(CGPoint(x: Int(nodePos.x) - Int(loc.x), y: Int(nodePos.y) - Int(loc.y)), offset: 1)
            }
        }
        
        
        //quadrants 3 and 4
        if(Int(loc.x) < Int(nodePos.x)) {
            
            //quadrant 3
            if(Int(loc.y) < Int(nodePos.y)) {
                calculateAngle(CGPoint(x: Int(loc.x) - Int(nodePos.x), y: Int(loc.y) - Int(nodePos.y)), offset: 2)
            }
            
            //quadrant 4
            if(Int(loc.y) > Int(nodePos.y)) {
                calculateAngle(CGPoint(x: Int(nodePos.x) - Int(loc.x), y: Int(nodePos.y) - Int(loc.y)), offset: 3)
            }
            
            
            
        }
    }
    
    func calculateDistance(origin : CGPoint, target: CGPoint) -> CGFloat {
        var difference : CGPoint = CGPoint(
            x:
                target.x - origin.x,
            y:
                target.y - origin.y
            )
        
        
        if(difference.x < 0.0)
        {
            difference.x *= -1.0;
        }
        if(difference.y < 0.0) {
            difference.y *= -1.0;
        }
        
        // we now have the absolute values of the legs of the triangle
        // therefore, difference.x ^ 2 + difference.y ^ 2 should = h^2
        
        difference.x = pow(difference.x, 2.0);
        difference.y = pow(difference.y, 2.0);
        
        var h = sqrt(difference.x + difference.y);
        
        println("\(h)");
        
        return h;
    }
    
    func calculateAngle(origin: CGPoint, target: CGPoint) -> CGFloat {
        if(origin.y == target.y)
        {
            // these two are on a straight line, therefore angle should be
            //      on either side of a 180 flat line
            
            if(origin.x > target.x)
            {
                // straight line between quad 1 and 2
                println("straight line between quad 1 and 2")
                return -3.0 * 1.570796
            }
            
            if(origin.x < target.x)
            {
                //straight line between 3 and 4
                NSLog("straight line between 3 and 4")
                return -1.0 * 1.570796
            }
        }
        
        if(origin.x == target.x)
        {
            if(origin.y > target.y)
            {
                // straight line between 2 and 3
                NSLog("Straight line between 2 and 3")
                return -1.570796 * 2.0
            }
            
            if(origin.y < target.y)
            {
                //straight line between 1 and 4
                NSLog("Straight line between 1 and 4");
                return 0.0
            }
        }
        
        var offset = 0;
        if(target.x > origin.x)
        {
            // this means this is either in quadrant 1 or 2
            if(target.y > origin.y)
            {
                //quadrant 1
                offset = 0;
            }
            if(target.y < origin.y) {
                //quadrant 2
                offset = 1
            }
        }
        
        if(target.x < origin.x)
        {
            if(target.y < origin.y)
            {
                //quadrant 3
                offset = 2
            }
            if(target.y > origin.y)
            {
                //quadrant 4
                offset = 3
            }
        }
        
        return calculateAngle(CGPoint(x: target.x - origin.x, y: target.y - origin.y), offset: offset)
    
//        return -1.570796
    }
    
    func calculateAngle(dxDy : CGPoint, offset : Int) -> CGFloat {

        var oppAdj : CGFloat = CGFloat(0.0);
        var coeff = 1;
        if(offset % 2 == 0)
        {
            oppAdj = CGFloat(dxDy.x / dxDy.y)
        }
        else
        {
            oppAdj = CGFloat(dxDy.y / dxDy.x)
            coeff = -1
        }
        
//        oppAdj = CGFloat(dxDy.x / dxDy.y)
        if(oppAdj < 0.0)
        {
            oppAdj *= -1
        }
        
        
        let result = atan(oppAdj)
        
        println(String(format:"%f", result))
        
//        tank.BaseNode.zRotation = -result
//        tank.BaseNode.zRotation +=  CGFloat(CGFloat(coeff) * 1.570796 * CGFloat(offset))
        
        let rotateTo = -result + CGFloat(CGFloat(coeff) * 1.570796 * CGFloat(offset))
        
        tank.sprite.runAction(SKAction.rotateToAngle(rotateTo, duration: 0.5, shortestUnitArc: true))
        
        let ab = abs(tank.BaseNode.zRotation)
//        return -result
        return -result + CGFloat(CGFloat(coeff) * 1.570796 * CGFloat(offset))
        
    }

    func rotateJoystick(loc: CGPoint)
    {
        let dist = calculateDistance(joystick.BaseNode.position, target: loc)
        
        
        
        
        //                let innerOffset = CGPoint(
        //                    x:
        //                    loc.x - joystick.BaseNode.position.x,
        //                    y:
        //                                        loc.y - joystick.BaseNode.position.y
        ////                    0.0
        //                )
        
        let innerOffset = CGPoint(x: 0.0, y:
            (dist < 25.0) ? dist : 25.0)
        joystick.innerCircle.position = innerOffset;
        
        let angle = calculateAngle(joystick.BaseNode.position, target: loc)
        
        //                joystick.BaseNode.zRotation = angle
        joystick.BaseNode.runAction(SKAction.rotateToAngle(angle, duration: 0.1, shortestUnitArc: true))
    }
    
    override func touchesMoved(touches: Set<NSObject>, withEvent event: UIEvent) {

        if(motionManager.deviceMotionActive)
        {
            let accelData =
            String(format: "roll: %f, pitch: %f, yaw: %f",
                motionManager.deviceMotion.attitude.roll,
                motionManager.deviceMotion.attitude.pitch,
                motionManager.deviceMotion.attitude.yaw)

//            
//            String(format: "roll: %f, pitch: %f, yaw: %f",
//                attitudeOrigin.roll,
//                attitudeOrigin.pitch,
//                attitudeOrigin.yaw)
            
            println(accelData)
            return
        }
        
        
        for touch in (touches as! Set<UITouch>)
        {
            let loc = touch.locationInNode(self)
            
            rotateJoystick(loc);
            
            let deadRadius : CGFloat = 15.0
            
            if touch == moveTouch
            {
                if(loc.x >= moveOrigin.x + deadRadius && lastMove.x >= moveOrigin.x + deadRadius)
                {
                    return;
                }
                
                if(loc.x <= moveOrigin.x - deadRadius && lastMove.x <= moveOrigin.x - deadRadius)
                {
                    return;
                }
                
                currentShip.BaseNode.removeActionForKey("move")
                
                lastMove = loc
                if loc.x >= moveOrigin.x + deadRadius
                {
                    let dest = CGPoint(
                        x: size.width - currentShip.sprite.size.width,
                        y: currentShip.BaseNode.position.y)
                    
                    let move = SKAction.moveTo(dest, duration: 2)
                    currentShip.BaseNode.runAction(move, withKey:  "move")
                }
                if loc.x <= moveOrigin.x - deadRadius
                {
                    let dest = CGPoint(
                        x: currentShip.sprite.size.width,
                        y: currentShip.BaseNode.position.y)
                    
                    let move = SKAction.moveTo(dest, duration: 2)
                    currentShip.BaseNode.runAction(move, withKey:  "move")
                }
                if loc.x < moveOrigin.x + deadRadius && loc.x > moveOrigin.x - deadRadius
                {
                    currentShip.BaseNode.removeActionForKey("move")
                }
            }
        }
    }
    
    func handleMovement(touch : UITouch) {
        moveTouch = touch
        moveOrigin = touch.locationInNode(self)
        lastMove = moveOrigin
    }
    
    func handleWeapons(touch : UITouch) {
        fireTouch = touch
        var weaponNumber = 0
        if fireTouch.locationInNode(self).x > size.width * 0.75
        {
            weaponNumber = 1
        }
        
        if(weaponNumber >= currentShip.weapons.count)
        {
            return
        }
        
        let openFire = SKAction.runBlock({
            self.openFire(weaponNumber)
        })
        
        let weapon = currentShip.weapons[weaponNumber];
        let wait = SKAction.waitForDuration(0.20)
        let seq = SKAction.repeatActionForever(SKAction.sequence([openFire, wait]))
        self.runAction(seq, withKey: "OpenFire")
    }
    
    override func touchesEnded(touches: Set<NSObject>, withEvent event: UIEvent) {
        
        for touch in (touches as! Set<UITouch>)
        {
            joystick.BaseNode.removeFromParent();
            
            if touch == fireTouch
            {
                self.removeActionForKey("OpenFire")
            }
            if touch == moveTouch
            {
                currentShip.BaseNode.removeActionForKey("move")
                moveTouch = UITouch()
                moveOrigin = CGPointZero
            }
        }
    }
    
    func AddBubble(loc : CGPoint, damage : Double, crit: Bool) {
        var isBub = true;
        if(isBub)
        {
            let bub = DamageBubble(text: "");
        
            bub.Node.position = loc;
            bub.text = String(format:"%d", Int(damage))
            bub.Crit = crit
            self.addChild(bub.Node)
            bub.runActions()
        }
        
        var isMBub = false
        if(isMBub)
        {
            let mBub = MultilineBubble()
            mBub.text = self.text

            mBub.runActions()
            
            mBub.position = loc;
            
            self.addChild(mBub)
        }
       
        let isTestBub = false;
        if(isTestBub)
        {
            let testBub = TextBoxWidthTestingBubble()
            testBub.runActions()
            testBub.position = loc
            
            self.addChild(testBub)
        }
        
        
    }
    func didBeginContact(contact: SKPhysicsContact) {

        if(contact.bodyA.categoryBitMask == PhysicsCategory.Projectile)
        {
            let projSprite = (contact.bodyA.node as? SKSpriteNode)!
            
            for projectile in projectiles {
                if projectile.Sprite == projSprite
                {
                    let damage = projectile.Damage
                    
                    
                    AddBubble(projSprite.position, damage: damage, crit: projectile.Crit)
                }
                
            }
            projSprite.removeFromParent()
        }
        else
        {
            let projSprite = (contact.bodyB.node as? SKSpriteNode)!
            for projectile in projectiles {
                if projectile.Sprite == projSprite
                {
                    let damage = projectile.Damage
                    AddBubble(projSprite.position, damage: damage, crit: projectile.Crit)
                    
                    println(projSprite.position)
                }
                
            }
            projSprite.removeFromParent()
        }
        

        
        
    }
    func openFire(weaponNumber : Int) {
//        var ship = ships[shipIterator]
        shotCounter++;
        if(shotCounter > 5)
        {
            if(currentShip.weapons.count > 0)
            {
                currentShip.weapons[0] = MachineGun()
            }
        }
        
        var projectiles : [ProjectileProtocol] = []
    
        projectiles = currentShip.AttemptToFire(weaponNumber)
        
        for projectile in projectiles
        {
            var weaponMuzzle
            = CGPoint(
                x: currentShip.BaseNode.position.x + projectile.Offset.x
                ,
                y: currentShip.BaseNode.position.y + (currentShip.sprite.size.height / 2) + projectile.Offset.y
            )
            
            projectile.Sprite.zPosition = -1
            
            projectile.Sprite.position = weaponMuzzle
            
            projectile.Sprite.physicsBody = SKPhysicsBody(rectangleOfSize: projectile.Sprite.size)
            projectile.Sprite.physicsBody?.dynamic = true
            projectile.Sprite.physicsBody?.categoryBitMask = PhysicsCategory.Projectile
            projectile.Sprite.physicsBody?.contactTestBitMask = PhysicsCategory.EnemyShip
            projectile.Sprite.physicsBody?.collisionBitMask = 0
            
            
//            projectile.Sprite.zRotation = 3.14159
            var move = SKAction.moveBy(CGVector(
                dx: 0.0 + projectile.VelocityOffset.x
                ,
                dy: (size.height )), duration: Double(1.0 / CGFloat(projectile.Speed)))
            
            var selfDestruct = SKAction.runBlock(
                {
                    projectile.Sprite.removeFromParent()
                })
            var seq = SKAction.sequence([move, selfDestruct])
            projectile.Sprite.runAction(seq)
            
            self.projectiles.append(projectile)
            self.addChild(projectile.Sprite)

        }
    }
}