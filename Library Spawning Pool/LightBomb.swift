//
//  LightBomb.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 6/30/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class LightBomb : WeaponProtocol, VehicalLibraryBaseProtocol {
    var Name : String
    var Ammo : Int = 100
    var Speed : Double
    var Offset : [CGPoint]
    var VelocityOffset : [CGPoint]
    init()
    {
        Name = "Light Bomb"
        Speed = 1.0

        Offset = [CGPoint(x: 0.0, y: 0.0)]
        VelocityOffset = [CGPoint(x: 0.0, y:0.0)]
    }
    
    func getProjectile() -> [ProjectileProtocol] {
        var projectile = LightBombProjectile(offset: CGPoint(x: 0.0, y: 0.0), VelocityOffset: VelocityOffset[0])
        let spin = SKAction.rotateByAngle(1.570796, duration: 0.05)
        
        let repeat = SKAction.repeatActionForever(spin)
        projectile.Sprite.runAction(repeat)
        return [projectile]
    }
    
    func getNewInstance() -> VehicalLibraryBaseProtocol {
        return LightBomb()
    }
 
}