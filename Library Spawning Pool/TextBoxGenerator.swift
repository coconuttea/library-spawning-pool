//
//  TextBoxGenerator.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/13/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class TextBoxGenerator : AvatarGeneratorProtocol {
    var Name : String
    var BaseNode : SKNode
    var Options : [SKLabelNode]
    init()
    {
        Name = "SKLabelThing"
        BaseNode = SKNode()
        Options = []
        let optionStrings = [
            "Hello",
            "World",
            "this",
            "is",
            "a",
            "test",
            "of",
            "the",
            "labelnode",
            "system"
        ]
        
        for optionString in optionStrings {
            let label = SKLabelNode(text: optionString)
            Options.append(label)
            label.fontName = "HelveticaNeue";
            label.fontSize = 20.0
        }
        
        let offset : CGFloat = -20.0
        
        var i = 0
        for option in Options {
            option.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
//            option.position.x = size.width / 2
            option.position.y = CGFloat(i) * offset
            
            BaseNode.addChild(option)
            i++;
        }

        
    }
    
    func UpdateAvatar() {
        
    }
}