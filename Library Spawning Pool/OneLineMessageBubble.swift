//
//  OneLineMessageBubble.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/8/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class OneLineMessageBubble : BubbleProtocol {
    var text : String = ""
    var duration : Double = 1.0
    var Crit : Bool = false
    var sprites : [SKNode] = []
    var Node : SKNode = SKNode()
    var label = SKLabelNode(text : "")
    init(text : String) {
        self.text = text;
        
        label = SKLabelNode(text: text)
        label.fontColor = UIColor(red: 1.0, green: 0.1, blue: 0.1, alpha: 1.0)
        label.fontSize = 32.0
        label.fontName = "HelveticaNeue-Bold"
        label.zPosition = 2
        Node.addChild(label)
        
    }
    
    func runActions() {
        Node.runAction(SKAction.sequence([
            SKAction.moveBy(CGVector(dx: 0.0, dy: 100.0), duration: duration),
            SKAction.runBlock({self.Node.removeFromParent()})
            ]))
        Node.runAction(SKAction.runBlock({
            self.label.color = UIColor(red: 1.0, green: 0.0, blue: 0.0, alpha: 1.0)
        }));
    }
}