//
//  SpiderTank.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/11/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class SpiderTank : ShipProtocol {
    var ShipName : String = "Spider Tank"
    var weapons : [WeaponProtocol] = []
    var BaseNode : SKNode = SKNode()
    var sprite : SKSpriteNode
    var VelocityMultiplier : Double = 0.75
    
    var Orbitals : [OrbitalProtocol] = []
    var Legs : [SKSpriteNode] = []
    
    func AttemptToFire(weaponNumber : Int) -> [ProjectileProtocol] {

        var projectiles : [ProjectileProtocol] = []
        if(weaponNumber < 0)
        {
            return projectiles
        }
        
        if(weaponNumber > weapons.count - 1)
        {
            return AttemptToFire(weaponNumber - 1);
        }
        
        let weapon = weapons[weaponNumber];
        
        for proj in weapon.getProjectile()
        {
            projectiles.append(proj)
        }
        
        return projectiles
        
    }
    
    init() {
        sprite = SKSpriteNode(imageNamed: "tankBeige_outline.png");

        
        for i in 0..<4
        {
            let leg = SKSpriteNode(imageNamed: "barrelBeige_outline.png")
//            leg.anchorPoint = CGPoint(x: 0.5, y: 0.5)
            leg.zPosition = 1
            sprite.addChild(leg);
            leg.zRotation = 1.570796
            leg.runAction(
                SKAction.repeatActionForever(
                SKAction.sequence([
                    SKAction.rotateByAngle(1.570796, duration: 1.0),
                    SKAction.rotateByAngle(-1.570796, duration: 1.0)
                ])))
            
            leg.position.y = CGFloat(i - 2) * (sprite.size.height / 4)
            leg.position.x = 0.0
        }
        
        for i in 0..<4
        {
            let leg = SKSpriteNode(imageNamed: "barrelBeige_outline.png")
//            leg.anchorPoint = CGPoint(x: 0.5, y: 0.95)
            leg.zPosition = 1
            sprite.addChild(leg);
            leg.zRotation = -1.570796
            leg.runAction(
                SKAction.repeatActionForever(
                    SKAction.sequence([
                        SKAction.rotateByAngle(-1.570796, duration: 0.5),
                        SKAction.rotateByAngle(1.570796, duration: 0.5)
                        ])))
            
            leg.position.y = CGFloat(i - 2) * (sprite.size.height / 4)
            leg.position.x = sprite.size.width
            
        }
        
        sprite.zPosition = 2

        
        BaseNode.addChild(sprite)
        AnimateLimbs()
    }
    
    func AnimateLimbs() {
        
    }

}