//
//  AvatarDemoScene.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/12/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class AvatarDemoScene : SKScene {
    
    var label : [SKLabelNode] = []
//    let textBox = TextBoxGenerator()
    let textBox = TextBoxGenerator()
    let offset : CGFloat = 30.0
    override func didMoveToView(view: SKView) {
        initGame()
    }
    
    func initGame() {
        backgroundColor = UIColor(red: 0.1, green: 0.75, blue: 0.1, alpha: 1.0)
        textBox.BaseNode.position.x = size.width / 2
        textBox.BaseNode.position.y = size.height / 2
        self.addChild(textBox.BaseNode)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        for touch in (touches as! Set<UITouch>) {

            let node = nodeAtPoint(touch.locationInNode(self))
            
            if( node == textBox.BaseNode)
            {
                println("basenode")
            }
            
            for option in textBox.Options {
                if node == option {
                    println(option.text)
                }
            }
            
        }
    }
}