//
//  BackgroundTile.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/3/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class BackgroundTile : FloorTileProtocol {
    var Sprite : SKSpriteNode
    
    init(backgroundImage : String, position : CGPoint) {
        Sprite = SKSpriteNode(imageNamed:backgroundImage)
        Sprite.position = position
    }
}