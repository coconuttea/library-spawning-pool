//
//  TripleLaserProjectile.swift
//  Library Spawning Pool
//
//  Created by Jonathan Wong on 7/2/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class TripleLaserProjectile : ProjectileProtocol {
    var BaseNode : SKNode
    var Sprite : SKSpriteNode
    var Damage : Double = 17.0
    var Speed : Double = 1.0
    var Crit : Bool = false
    var CritChance : Double = 25.0
    var CritModifier : Double = 2.0
    var Offset : CGPoint
    var VelocityOffset : CGPoint
    
    init(offset : CGPoint, VelocityOffset : CGPoint)
    {
        BaseNode = SKNode()
        let SpriteImage = "laserGreen08.png"
        Sprite = SKSpriteNode(imageNamed: SpriteImage)
        Offset = offset
        self.VelocityOffset = VelocityOffset
        
        if( (Double(random()) % 100) <= CritChance)
        {
            Crit = true
            Damage *= CritModifier
        }
    }
}