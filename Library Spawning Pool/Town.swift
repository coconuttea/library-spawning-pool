//
//  Town.swift
//  Library Spawning Pool
//
//  Created by Matthew Wong on 7/3/15.
//  Copyright (c) 2015 Jonathan Wong. All rights reserved.
//

import Foundation
import SpriteKit

class Town : SettingProtocol {
    var Settings : [SettingProtocol]
    var Characters : [CharacterProtocol]
    var FloorTiles : [FloorTileProtocol]
    var Name : String
    init(name : String) {
        Settings = []
        Characters = []
        FloorTiles = []
        Name = name
        
        Characters.append(Hero(heroImage: "rpgTile200.png", position: CGPoint(x:500, y:500)))
        FloorTiles.append(BackgroundTile(backgroundImage: "rpgTile000.png", position: CGPoint(x: 500.0, y: 500.0)))
    }
}